<?php // -*- php -*-

$header = <<<EOF
EOF;


$finder = PhpCsFixer\Config::create()
        ->getFinder()
        ->notName('config.php');

return PhpCsFixer\Config::create()
    ->setFinder($finder)
    ->setRules([
        '@PSR2' => true,
        'header_comment' => [
            'commentType' => 'PHPDoc',
            'header' => $header,
            'separate' => 'none'
        ],
        // 'strict_param' => true,
        'array_syntax' => array('syntax' => 'short'),
        'ordered_imports' => true,
        'no_unused_imports' => true
    ]);
