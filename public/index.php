<?php
/**
 * API接口文档：http://spec.openapis.org/oas/v3.0.3
 * data types : https://github.com/OpenAPITools/openapi-generator/issues/955
 * data types : http://spec.openapis.org/oas/v3.0.3#data-types
 *
 * 布隆过滤器：https://www.jianshu.com/p/2104d11ee0a2
 * 布隆过滤器-删除：https://cloud.tencent.com/developer/article/1136056
 *
 *
 * <!-- https://fontawesome.com/cheatsheet -->
 */


if (!defined('ROOT')) {
    define('ROOT', dirname(dirname(__FILE__)));
}

ini_set('date.timezone', 'Asia/Shanghai');

//session_start();

include ROOT . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$app = new \Framework\Frame();
$app->run();
