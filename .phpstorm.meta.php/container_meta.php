<?php


namespace PHPSTORM_META {

    use Psr\Http\Message\ResponseInterface;

    override(\Framework\Foundation\Container\Container::get(), map([
        'serverRequest' => \Framework\Foundation\Request\ServerRequest::class,
        'response' => ResponseInterface::class
    ]));

//    expectedArguments(
//        \Framework\Foundation\Container\Container::get(),
//        0,
//        'serverRequest',
//        'response',
//    );

}
