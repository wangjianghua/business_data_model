#!/usr/bin/env bash

# https://qq52o.me/2688.html
composer install --no-dev
composer dump-autoload -o