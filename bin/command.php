<?php
// application.php

define('ROOT', dirname(__DIR__));

require ROOT . '/vendor/autoload.php';

\Framework\Foundation\Ini::parseIniFile(ROOT . '/.ini');


use Symfony\Component\Console\Application;

$application = new Application();

// ... register commands

$dir = scandir(ROOT . '/app/Command');
foreach ($dir as $v) {
    if (!in_array($v, ['.', '..']) && substr($v, -4) == '.php') {
        $class = '\App\Command\\' . rtrim($v, '.php');
        $application->add(new $class);
    }
}

$application->run();
