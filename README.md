# 项目名称
业务系统事件-数据模型管理系统


# 项目说明
开发经验丰富的开发者都认同一件事是：项目bug50%上以是数据问题导致。  
经年积月和开发人员的轮换导致当下开发人员对业务系统理解不一至，不正确会高概率产生BUG.  
接口文档，详细设计都不能简单明地说明系统业务事件操作和依赖的数据和数据结构。  
  
因此，我开发了这个系统。意在开发新功能或重构时校验对比历史版本事件数据结构和值。  
维护业务事件迭代的多个数据结构版本。  


 
# 项目技术实现
### 数据流和事件
 canal消费mysql-binlog，获得数据库的插入，更新，删除等数据。  
 根据mysql-binlog解析出来的数据，组合多个库表的插入，更新操作组合成为一个业务事件。  
### 数据流和事件对比，事件和事件对比 
 新生成的数据流和已有业务事件对比更新的表，更新的字段，更新的值数据异同。
 
# 快速入门
## 安装
#### 项目依赖
 - [canal - 基于 MySQL 数据库增量日志解析，提供增量数据订阅和消费](https://github.com/alibaba/canal)
 - php7.0+
 - composer
### 安装canal
 > 请参考 https://github.com/alibaba/canal
### 安装(business_data_model)业务事件数据模型系统代码
 > composer create-project business_event/business_data_model

 > composer install
 
### 导入数据库解构
 > business_event.sql
### 配置与启动

#### 配置
如项目下没有.ini文件， 根据example拷贝一份。
> cp .ini.example .ini

配置 mysql 和 canal项 

#### 启动
> php bin/command.php canal:client

或者

> ./bin/phpcmd canal:client
 
# 简单示例DEMO

### 首先需要有数据流内空， 根据文档配置好canal, 启动client读取canal数据
```bash
./bin/phpcmd canal:client
```
### 生成的data_stream数据流如下,可标识出同事务插入，列新，删除的数据。支持库，表，事务的搜索。
![事件数据流](/public/dist/images/event_stream.jpg)
### 根据数据流，创建事件
![创建事件](/public/dist/images/event_create.jpg)
### 查看事件或数据流内容详情
![事件和数据内容详情](/public/dist/images/data_model_effect.jpg)
### 数据流或事件对比创建好的事件
![数据流或事件对比事件](/public/dist/images/data_model_diff.jpg)

## 更新数据异同对比示例
![数据对比](/public/dist/images/diff_update_column.jpg)
![数据对比](/public/dist/images/diff_update_value.jpg)