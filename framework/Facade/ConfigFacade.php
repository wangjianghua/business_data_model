<?php

namespace Framework\Facade;

use Framework\Foundation\Config;

/**
 * Class ConfigFacade
 * @package Framework\Facade
 *
 * @method static mixed get($scope, $key, $default=null)
 * @method static void set(string $key, $value)
 */
class ConfigFacade extends Facade
{
    protected static $contractClass = Config\ConfigCompose::class;
}
