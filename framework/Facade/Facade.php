<?php

namespace Framework\Facade;

class Facade
{
    protected static $configInstance;


    public static function __callStatic($name, $arguments)
    {
        $class = self::getInstance();
        return call_user_func_array([$class, $name], $arguments);
    }

    public static function getInstance()
    {
        if (is_null(self::$configInstance)) {
            self::$configInstance = new static::$contractClass;
        }

        return self::$configInstance;
    }

//    public static function __callStatic($name, $arguments)
//    {
//        $class = get_called_class();
//        echo '<pre>';
//        print_r($class);
//        exit('</pre>');
//        // TODO: Implement __callStatic() method.
//    }
}
