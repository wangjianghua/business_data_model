<?php

namespace Framework\Foundation\Config;

use clagiordano\weblibs\configmanager\ConfigManager;

class ConfigCompose implements ConfigInterface
{
    /**
     * @var $configScope ConfigManager[]
     */
    protected $configScope = [];


    public function get($scope, $key = null, $default = null)
    {
        if (is_null($scope)) {
            return $this->configScope;
        }

        if (!isset($this->configScope[$scope])) {
            $this->configScope[$scope] = new ConfigManager($this->getFilePath($scope));
        }
//        if (is_null($key)) {
//            return $this->configScope[$scope]->getValue($key);
//        }

        return $this->configScope[$scope]->getValue($key, $default);
    }

    public function set($scope, $key, $value)
    {
        if (is_null($scope)) {
            return false;
        }

        if (!isset($this->configScope[$scope])) {
            $this->configScope[$scope] = new ConfigManager();
        }

        $this->configScope[$scope]->setValue($key, $value);

        return true;
    }

    public function getScopeAllConfig($scope, $default = null)
    {
        if (isset($this->configScope[$scope])) {
            return $this->configScope[$scope]->getValue(null);
        }

        return $default;
    }

    public function getFilePath($configFile)
    {
        return ROOT . '/app/config/' . $configFile . '.php';
    }
}
