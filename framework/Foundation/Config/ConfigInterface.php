<?php

namespace Framework\Foundation\Config;

interface ConfigInterface
{
    public function get($scope, $key, $default);

    public function set($scope, $key, $value);
}
