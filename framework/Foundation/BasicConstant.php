<?php

namespace Framework\Foundation;

/**
 * Class BasicEnum
 * @package App\Common\Rest
 * @see https://www.php.net/manual/zh/class.splenum.php
 */
class BasicConstant
{
    private static $constCacheArray = null;

    private function __construct()
    {
        /*
          Preventing instance :)
        */
    }

    public static function getConstants()
    {
        if (self::$constCacheArray == null) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict = true);
    }

    public static function getCodeMessage(int $value)
    {
        $constants = self::getConstants();
        $key = array_keys($constants, $value);

        if (empty($key)) {
            return 'Message Undefined';
        }
        $worlds = str_replace('_', ' ', $key[0]);

        return ucwords(strtolower($worlds));
    }
}
