<?php

namespace Framework\Foundation;

final class Ini
{
    protected static $parsed = false;
    protected static $envInfo = [];

    public static function get(string $key = null, $default = null)
    {
        if (is_null($key)) {
            return self::$envInfo;
        }

        if (false === strpos($key, '.')) {
            return self::$envInfo[$key] ?? $default;
        }

        $keys = explode('.', $key);
        $subEnvInfo = self::$envInfo;
        foreach ($keys as $k) {
            if (!isset($subEnvInfo[$k])) {
                return $default;
            }
            $subEnvInfo = $subEnvInfo[$k];
        }
        return $subEnvInfo;
    }

    public static function parseIniFile($file)
    {
        if (file_exists($file)) {
            self::$envInfo = parse_ini_file($file, true);
        }
    }
}
