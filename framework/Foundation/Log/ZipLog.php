<?php

namespace Framework\Foundation\Log;

use Framework\Foundation\Zipkin\ZipkinAssistant;
use Zipkin\Span;

class ZipLog
{

    /**
     * @var ZipkinAssistant
     */
    private static $zipkinAssistant;

    private static $flushAllTime = true;

    public static function setZipkin(ZipkinAssistant $zipkinAssistant)
    {
        self::$zipkinAssistant = $zipkinAssistant;
    }

    public static function info($message, array $context = array())
    {
        $span = self::$zipkinAssistant->autoGetSpan();
        $span->start();
        self::spanLog($span, $message, $context);
        $span->finish();

        if (self::$flushAllTime) {
            self::$zipkinAssistant->getTracer()->flush();
        }
    }

    public static function spanStart()
    {
        $span = self::$zipkinAssistant->autoGetSpan();
        $span->start();
        return $span;
    }

    public static function spanLog(Span $span, $message, array $context = array())
    {
        if (empty($context)) {
            $span->annotate($message, \Zipkin\Timestamp\now());
        } else {
            $span->tag($message, json_encode($context));
        }
    }

    public static function spanFinish(Span $span)
    {
        $span->finish();
        if (self::$flushAllTime) {
            self::$zipkinAssistant->getTracer()->flush();
        }
    }

//    public static function emergency($message, array $context = array())
//    {
//        self::log(Logger::EMERGENCY, $message, $context);
//    }
//
//    public static function alert($message, array $context = array())
//    {
//        self::log(Logger::ALERT, $message, $context);
//    }
//
//    public static function critical($message, array $context = array())
//    {
//        self::log(Logger::CRITICAL, $message, $context);
//    }
//
//    public static function error($message, array $context = array())
//    {
//        self::log(Logger::ERROR, $message, $context);
//    }
//
//    public static function warning($message, array $context = array())
//    {
//        self::log(Logger::WARNING, $message, $context);
//    }
//
//    public static function notice($message, array $context = array())
//    {
//        self::log(Logger::NOTICE, $message, $context);
//    }
//
//    public static function info($message, array $context = array())
//    {
//        self::log(Logger::INFO, $message, $context);
//    }
//
//    public static function debug($message, array $context = array())
//    {
//        self::log(Logger::DEBUG, $message, $context);
//    }
//
//    public static function log($level, $message, array $context = array())
//    {
//        $span = self::$zipkinAssistant->autoGetSpan();
//        $span->start();
//        if (empty($context)) {
//            $span->annotate(Logger::getLevelName($level) . ': ' . $message, \Zipkin\Timestamp\now());
//        } else {
//            $span->tag(Logger::getLevelName($level) . ': ' . $message, json_encode($context));
//        }
//        $span->finish();
//
//        if (self::$flushAllTime) {
//            self::$zipkinAssistant->getTracer()->flush();
//        }
//    }
}
