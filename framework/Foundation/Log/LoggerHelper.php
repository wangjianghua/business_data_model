<?php

namespace Framework\Foundation\Log;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerHelper
{
    public function getLogger($channel = 'default', $daily = true)
    {
        if ($daily) {
            $file = ROOT . '/storage/logs/' . $channel . '_' . date('Y-m-d') . '.log';
        } else {
            $file = ROOT . '/storage/logs/' . $channel . '.log';
        }

        return new Logger(100, [new StreamHandler($file, Logger::DEBUG)]);
    }
}
