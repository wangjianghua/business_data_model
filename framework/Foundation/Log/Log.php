<?php

namespace Framework\Foundation\Log;

/**
 * Class Log
 * @method static bool log($level, $message, array $context = array())
 * @method static bool debug($message, array $context = array())
 * @method static bool info($message, array $context = array())
 * @method static bool notice($message, array $context = array())
 * @method static bool warn($message, array $context = array())
 * @method static bool warning($message, array $context = array())
 * @method static bool err($message, array $context = array())
 * @method static bool error($message, array $context = array())
 * @method static bool crit($message, array $context = array())
 * @method static bool critical($message, array $context = array())
 * @method static bool alert($message, array $context = array())
 * @method static bool emerg($message, array $context = array())
 * @method static bool emergency($message, array $context = array())
 * @package Framework\Foundation\Log
 */
class Log
{
    public static $logger;

    public static function init()
    {
        self::$logger = (new LoggerHelper)->getLogger();
    }

    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([self::$logger, $name], $arguments);
    }
}
