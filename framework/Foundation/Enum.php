<?php

namespace Framework\Foundation;

class Enum
{
    protected $enumsData;


    public function getEnumKeys()
    {
        return $this->enumsData['keys'];
    }

    public function getEnumDesc()
    {
        return $this->enumsData['desc'];
    }

    public function getDescByNum($enum)
    {
        return $this->enumsData['desc'][$enum] ?? null;
    }

    public function getNumByDesc($desc)
    {
        return array_search($desc, $this->enumsData['desc']);
    }

    public function getKeyByNum($enum)
    {
        return array_search($enum, $this->enumsData['keys']);
    }

    public function getNumByKey($key)
    {
        return $this->enumsData['keys'][$key] ?? null;
    }

    public function getKeyByDesc($desc)
    {
        $num = $this->getNumByDesc($desc);
        if ($num === false) {
            return $num;
        }
        return $this->getKeyByNum($num);
    }

    public function getDescByKey($key)
    {
        $num = $this->getNumByKey($key);
        if ($num === false) {
            return $num;
        }
        return $this->getDescByNum($num);
    }

    public function getAllTerm()
    {
        return $this->enumsData;
    }

    public function listTermName()
    {
        return array_keys($this->enumsData);
    }
}
