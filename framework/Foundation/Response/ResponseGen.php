<?php

namespace Framework\Foundation\Response;

use Framework\Foundation\Container\Container;
use function GuzzleHttp\Psr7\stream_for;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class ResponseGen
{
    public static function locationJump($url)
    {
        $response = self::getDefaultResponse();
        return $response->withHeader('Location', $url);
    }

    public static function error($code = 0, $msg = 'error', $data = [])
    {
        return self::responseJson($code, $msg, $data);
    }

    public static function success($data = [], $msg = 'success', $code = 200)
    {
        return self::responseJson($code, $msg, $data);
    }

    public static function responseJson(
        int $code = 0,
        string $msg = '',
        $data = [],
        $headers = [
            'Content-Type' => 'application/json'
        ]
    ): ResponseInterface {
        $response = self::getDefaultResponse();
        if (!empty($headers)) {
            foreach ($headers as $key => $value) {
                $response = $response->withHeader($key, $value);
            }
        }

        $json['code'] = $code;
        $json['msg'] = $msg;
        $json['data'] = $data;

        return $response->withHeader('Content-Type', 'application/json')->withBody(stream_for(json_encode(
            $json,
            JSON_UNESCAPED_UNICODE
        )));
    }

    public static function raw($data, ResponseInterface $response = null)
    {
        if (is_null($response)) {
            $response = self::getDefaultResponse();
        }
        return $response->withBody(stream_for($data));
    }

    public static function getDefaultResponse()
    {
        $response = Container::instance()->get('response');
        if (is_null($response)) {
            $response = new \GuzzleHttp\Psr7\Response();
        }

        return $response;
    }

    public static function display($response)
    {
        switch ($response) {
            case $response instanceof Response:
                $response->send();
                break;
            case $response instanceof ResponseInterface:
                self::outHeaders($response->getHeaders());
                echo $response->getBody()->getContents();
                break;
        }
//        if($response instanceof Response){
//
//        }
//        if($response instanceof ResponseInterface){
//            self::outHeaders($response->getHeaders());
//            echo $response->getBody()->getContents();
//        }
    }

    private static function outHeaders(array $headers)
    {
        if (headers_sent()) {
            return false;
        }
        if (empty($headers)) {
            return true;
        }
        ksort($headers);
        $max = max(array_map('strlen', array_keys($headers))) + 1;
        foreach ($headers as $name => $values) {
            $name = ucwords($name, '-');
            foreach ($values as $value) {
                if (in_array($name, ["Access-Control-Allow-Credentials"])) {
                    header($name . ':true');
                    continue;
                }

                $header = sprintf("%-{$max}s %s\r\n", $name . ':', $value);
                header($header);
            }
        }

        return true;
    }
}
