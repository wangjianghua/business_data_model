<?php
/**
 * Created by PhpStorm.
 * User: owen
 * Date: 2020/7/18
 * Time: 3:41 PM
 */

namespace Framework\Foundation\Queue;

use Framework\Foundation\Queue\Interfaces\QueueHandler;

class AmqpQueueHandler implements QueueHandler
{
    public function push(string ...$msg)
    {
        // TODO: Implement push() method.
    }

    public function pop()
    {
        // TODO: Implement pop() method.
    }

    public function blockPop()
    {
        // TODO: Implement blockPop() method.
    }
}
