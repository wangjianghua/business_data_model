<?php

namespace Framework\Foundation\Queue;

use Framework\Foundation\Queue\Interfaces\QueueHandler;

class FIleQueueHandler implements QueueHandler
{
    /**
     * @var $redis \Redis;
     */
    public $file;


    public function __construct($file)
    {
        $this->file = $file;
    }

    public function push(string ...$msg)
    {
        return true;
    }

    public function pop()
    {
        return true;
    }

    public function blockPop()
    {
        return false;
    }
}
