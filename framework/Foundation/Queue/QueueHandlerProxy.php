<?php

namespace Framework\Foundation\Queue;

use Framework\Foundation\Queue\Interfaces\QueueHandler;

class QueueHandlerProxy implements QueueHandler
{
    /**
     * @var QueueHandler
     */
    public $queueHandler;

    public function __construct($queueHandler)
    {
        $this->queueHandler = $queueHandler;
    }

    public function push(string ...$msg): bool
    {
        return $this->queueHandler->push(...$msg);
    }

    public function pop()
    {
        return $this->queueHandler->pop();
    }

    public function blockPop()
    {
        // TODO: Implement blockPop() method.
    }
}
