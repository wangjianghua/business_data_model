<?php

namespace Framework\Foundation\Queue\Interfaces;

interface QueueHandler
{
    public function push(string ...$msg);

    public function pop();

    public function blockPop();
}
