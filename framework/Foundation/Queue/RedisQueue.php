<?php

namespace Framework\Foundation\Queue;

use Framework\Foundation\Queue\Interfaces\QueueHandler;

class RedisQueue implements QueueHandler
{


    /**
     * @var $redis \Redis;
     */
    protected $redis;

    protected $queueKey;

    public function __construct($redis, $queueKey = 'default_queue')
    {
        $this->redis = $redis;
        $this->queueKey = $queueKey;
    }

    public function setQueueKey(string $key)
    {
        $this->queueKey = $key;
    }

    public function push(string ...$msg)
    {
        return $this->redis->lPush($this->queueKey, ...$msg);
    }

    public function pop()
    {
        return $this->redis->rPop($this->queueKey);
    }

    public function blockPop()
    {
        return $this->redis->brPop([$this->queueKey], 30);
    }
}
