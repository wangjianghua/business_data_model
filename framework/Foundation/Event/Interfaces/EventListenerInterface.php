<?php

namespace Framework\Foundation\Event\Interfaces;

interface EventListenerInterface
{
    public function asynchronous(): bool;

    public function handleEvent(object $event): void;
}
