<?php

namespace Framework\Foundation\Event;

use Framework\Foundation\Container\Container;
use Psr\EventDispatcher\EventDispatcherInterface;

class EventLauncher
{
    public static function publish(object $event, EventDispatcherInterface $dispatcher = null)
    {
        if (is_null($dispatcher)) {
            $dispatcher = Container::instance()->get('eventDispatcher');
        }

        $dispatcher->dispatch($event);
    }
}
