<?php

namespace Framework\Foundation\Event;

class AsyncEventListenerRunner
{
    public function asyncDispatch($msg)
    {
        $arr = json_decode($msg, true);
        $event = unserialize($arr['event']);
        foreach ($arr['listeners'] as $listenerName) {
            $this->asyncHandleEvent($listenerName, $event);
        }
    }

    public function asyncHandleEvent($listenerName, $event)
    {
        try {
            /**
             * @var $listener EventListenerInterface
             */
            $listener = new $listenerName;
            $listener->handleEvent($event);
        } catch (\Exception $e) {
        }
    }
}
