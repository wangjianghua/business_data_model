<?php

namespace Framework\Foundation\Event;

use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class Event
 * @package Framework\Foundation\Event
 * @link https://www.cnblogs.com/youyoui/p/8610068.html
 * @link https://www.php.net/manual/zh/language.oop5.magic.php#object.wakeup
 */
class Event
{
    protected $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function __sleep()
    {
        return ["data"];
    }

    public function __wakeup()
    {
    }
}
