<?php

namespace Framework\Foundation\Event;

use Framework\Foundation\Event\Interfaces\EventListenerInterface;

class Listener implements EventListenerInterface
{
    protected $async;

    public function __construct(bool $async = false)
    {
        $this->async = $async;
    }

    public function setAsync(bool $type)
    {
        $this->async = $type;
    }

    public function asynchronous(): bool
    {
        return $this->async;
    }

    public function handleEvent(object $event): void
    {
    }
}
