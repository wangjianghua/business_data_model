<?php

namespace Framework\Foundation\Container\Exceptions;

use Psr\Container\ContainerExceptionInterface;

class NotFoundException extends \Exception implements ContainerExceptionInterface
{
}
