<?php

namespace Framework\Foundation\Entity;

use Framework\Foundation\Database\FDB;

class EntityBuilder
{
    public static function genByPkId($tableName, $id, $fields = null)
    {
        $query = FDB::from($tableName)->where($tableName . '_id', $id);
        if (!is_null($fields)) {
            $query->select($fields, true);
        }

        $info = $query->fetch();
        if (empty($info)) {
            return null;
        }

        return new BaseEntity($info);
    }

    public static function genByAttr($tableName, $fieldName, $id, $fields = null)
    {
        $query = FDB::from($tableName)->where($fieldName, $id);
        if (!is_null($fields)) {
            $query->select($fields, true);
        }

        $info = $query->fetch();
        if (empty($info)) {
            return null;
        }

        return new BaseEntity($info);
    }

    public static function genByObjectInfo($className, $info)
    {
        return new $className($info);
    }
}
