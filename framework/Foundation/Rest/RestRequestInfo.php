<?php


namespace Framework\Foundation\Rest;

class RestRequestInfo
{
    protected $routeInfo = [];

    protected $module = '';

    protected $version = '';

    protected $restRouteInfo = [];

    protected $restTargetInfo = [];


    /**
     * RestRequestInfo constructor.
     * @param array $routeInfo
     * ['method']
     * ['route']
     */
    public function __construct(array $routeInfo)
    {
        $this->routeInfo = $routeInfo;

        $route = ltrim($routeInfo['route'], '/');
        $routeArr = explode('/', $route);

        $this->module = $routeArr[0];
        $this->version = $routeArr[1];

        $this->restRouteInfo = [
            'resource' => $routeArr[2] ?? null,
            'id' => $routeArr[3] ?? 0,
            'relation' => $routeArr[4] ?? null
        ];

        $this->restTargetInfo = ResourceRecover::recover($routeInfo['ajax_method'], $this->restRouteInfo);
    }

    public function getRouteInfo(): array
    {
        return $this->routeInfo;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return mixed
     */
    public function getRestTargetInfo()
    {
        return $this->restTargetInfo;
    }

    public function getResource()
    {
        return $this->restTargetInfo['resource'] ?? null;
    }

    public function getId()
    {
        return $this->restTargetInfo['id'] ?? 0;
    }

    public function getRelation()
    {
        return $this->restTargetInfo['relation'] ?? null;
    }
}
