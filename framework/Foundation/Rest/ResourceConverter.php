<?php

namespace Framework\Foundation\Rest;

use App\Exceptions\FrameException;

class ResourceConverter
{
    public function autoConvert($method, $restRouteArray)
    {
        switch ($method) {
            case 'GET':
                return $this->getTransResourceAndAction($restRouteArray);
                break;
            case 'POST':
                return $this->postTransResourceAndAction($restRouteArray);
                break;
            case 'PUT':
                return $this->putTransResourceAndAction($restRouteArray);
                break;
            case 'DELETE':
                return $this->delTransResourceAndAction($restRouteArray);
                break;
        }
    }

    public function getTransResourceAndAction($params)
    {
        switch (count($params)) {
            case 1:
                $action = "getList";
                break;
            case 2:
                $action = "getInfo";
                break;
            case 3:
                $action = "getList";
                break;
            default:
                throw new FrameException("系统处理异常");
                break;
        }
        return $this->makeTarget($action, $params['resource'], $params['id'] ?? 0, $params['relation'] ?? null);
//        $data = [];
//        switch (count($params)) {
//            case 0: // 缺少资源
//                break;
//            case 1:
//                $data['action'] = 'getList';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                $data['id'] = 0;
//                $data['field'] = $data['resource'] . '_id';
//                break;
//            case 2:
//                $data['action'] = 'getInfo';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                $data['id'] = $params['id'];
//                $data['field'] = $data['resource'] . '_id';
//                break;
//            case 3:
//                $data['action'] = 'getList';
//                $data['resource'] = $this->targetResource(strtolower($params['resource']), strtolower($params['relation']));
//                $data['id'] = $params['id'];
//                $data['field'] = $data['resource'] . '_id';
//                break;
//        }
//        $data['object'] = $this->convertTableToObject($data['resource']);
//        return $data;
    }

    public function postTransResourceAndAction($params)
    {
        switch (count($params)) {
            case 1:
                $action = "add";
                break;
            case 2:
                $action = "edit";
                break;
//            case 3:
//                $action = "add";
//                break;
            default:
                throw new FrameException("系统处理异常");
                break;
        }
        return $this->makeTarget($action, $params['resource'], $params['id'] ?? 0, $params['relation'] ?? null);
//        $data = [];
//        switch (count($params)) {
//            case 1:
//                $data['action'] = 'add';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                break;
//            case 2:
////                $data['action'] = 'edit';
////                $data['resource'] = $params['resource'];
////                $data['id'] = $params['id'];
//                break;
//        }
//        $data['object'] = $this->convertTableToObject($data['resource']);
//
//        return $data;
    }

    public function putTransResourceAndAction($params)
    {
        switch (count($params)) {
            case 1:
                $action = "edit";
                break;
            case 2:
                $action = "edit";
                break;
            case 3:
                $action = "edit";
                break;
            default:
                throw new FrameException("系统处理异常");
                break;
        }
        return $this->makeTarget($action, $params['resource'], $params['id'] ?? 0, $params['relation'] ?? null);
//        $data = [];
//        switch (count($params)) {
//            case 1:
//                $data['action'] = 'edit';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                $data['id'] = 0;
//                $data['field'] = $data['resource'] . '_id';
//                break;
//            case 2:
//                $data['action'] = 'edit';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                $data['id'] = $params['id'];
//                $data['field'] = $data['resource'] . '_id';
//                break;
//            case 3:
//                $data['action'] = 'edit';
//                $data['resource'] = $this->targetResource(strtolower($params['resource']), strtolower($params['relation']));
//                $data['id'] = $params['id'];
//                $data['field'] = $data['resource'] . '_id';
////                $data['action'] = 'edit';
////                $data['resource'] = $this->targetResource($params['resource'], $params['relation']);
////                $data['id'] = $params['id'];
//                break;
//        }
//        $data['object'] = $this->convertTableToObject($data['resource']);
//
//        return $data;
    }

    public function delTransResourceAndAction($params)
    {
        switch (count($params)) {
            case 1:
                $action = "delete";
                break;
            case 2:
                $action = "delete";
                break;
            case 3:
                $action = "delete";
                break;
            default:
                throw new FrameException("系统处理异常");
                break;
        }
        return $this->makeTarget($action, $params['resource'], $params['id'] ?? 0, $params['relation'] ?? null);

//        $data = [];
//        switch (count($params)) {
//            case 1:
//                $data['action'] = 'delete';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                break;
//            case 2:
//                $data['action'] = 'delete';
//                $data['resource'] = $this->convertObjectToTable($params['resource']);
//                $data['id'] = $params['id'];
//                $data['field'] = $data['resource'] . '_id';
//                break;
//            case 3:
////                $data['action'] = 'del';
////                $data['resource'] = $this->targetResource($params['resource'], $params['relation']);
////                $data['id'] = $params['id'];
//                break;
//        }
//        $data['object'] = $this->convertTableToObject($data['resource']);
//
//        return $data;
    }

    public function convertTableToObject($resource)
    {
        return str_replace('_', '', ucwords($resource, '_'));
    }

    public function convertObjectToTable($objectName)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1_$2", $objectName));
    }

    public function targetResource($resource, $relate)
    {
        if ($resource == 'permission' && $relate == 'role') {
            return $relate . '_' . $resource;
        }

        if ($resource == 'role' && $relate == 'user') {
            return $relate . '_' . $resource;
        }

        return $resource . '_' . $relate;
    }

    public function makeTarget($action, $resource, $id = 0, $relation = null)
    {
        if (is_null($relation)) {
            $data['resource'] = $this->convertObjectToTable($resource);
        } else {
            $data['resource'] = $this->targetResource(strtolower($resource), strtolower($relation));
        }

        $data['action'] = $action;
        $data['id'] = $id;
        $data['field'] = $data['resource'] . '_id';
        $data['object'] = $this->convertTableToObject($data['resource']);
        return $data;
    }

    /**
     * @param $className
     * @return bool|int|string
     * @see https://www.php.net/manual/en/function.get-class.php
     */
    public function getClassName($className)
    {
        if ($pos = strrpos($className, '\\')) {
            return substr($className, $pos + 1);
        }
        return $pos;
    }
}
