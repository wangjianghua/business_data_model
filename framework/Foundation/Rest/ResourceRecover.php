<?php

namespace Framework\Foundation\Rest;

class ResourceRecover
{
    public static function recover($ajaxMethod, $params)
    {
        switch ($ajaxMethod) {
            case 'GET':
                return self::getTransResourceAndAction($params);
                break;
            case 'POST':
                return self::postTransResourceAndAction($params);
                break;
            case 'PUT':
                return self::putTransResourceAndAction($params);
                break;
            case 'DELETE':
                return self::delTransResourceAndAction($params);
                break;
        }
    }

    public static function getTransResourceAndAction($params)
    {
        $resource = $params['resource'];
        $id = $params['id'];
        $relation = $params['relation'];

        $action = 'getList';
        if (!empty($id)) {
            $action = 'getInfo';
        }

        if (!empty($relation)) {
            $action = 'getList';
        }

        return self::makeTarget($action, $resource, $id, $relation);
    }

    public static function postTransResourceAndAction($params)
    {
        $resource = $params['resource'];
        $id = $params['id'];
        $relation = $params['relation'];

        $action = 'add';
        if (!empty($id)) {
            $action = 'edit';
        }

        return self::makeTarget($action, $resource, $id, $relation);
    }

    public static function putTransResourceAndAction($params)
    {
        $resource = $params['resource'];
        $id = $params['id'];
        $relation = $params['relation'];

        $action = 'edit';

        return self::makeTarget($action, $resource, $id, $relation);
    }

    public static function delTransResourceAndAction($params)
    {
        $resource = $params['resource'];
        $id = $params['id'];
        $relation = $params['relation'];

        $action = 'delete';
        return self::makeTarget($action, $resource, $id, $relation);
    }


    public static function convertTableToObject($resource)
    {
        return str_replace('_', '', ucwords($resource, '_'));
    }

    public static function convertObjectToTable($objectName)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1_$2", $objectName));
    }

    public static function targetResource($resource, $relate)
    {
        if ($resource == 'permission' && $relate == 'role') {
            return $relate . '_' . $resource;
        }

        if ($resource == 'role' && $relate == 'user') {
            return $relate . '_' . $resource;
        }

        return $resource . '_' . $relate;
    }

    public static function makeTarget($action, $resource, $id = 0, $relation = null)
    {
        if (is_null($relation)) {
            $data['resource'] = self::convertObjectToTable($resource);
        } else {
            $data['resource'] = self::targetResource(strtolower($resource), strtolower($relation));
        }

        $data['action'] = $action;
        $data['id'] = $id;
        $data['field'] = $data['resource'] . '_id';
        $data['object'] = self::convertTableToObject($data['resource']);
        return $data;
    }

    /**
     * @param $className
     * @return bool|int|string
     * @see https://www.php.net/manual/en/function.get-class.php
     */
    public static function getClassName($className)
    {
        if ($pos = strrpos($className, '\\')) {
            return substr($className, $pos + 1);
        }
        return $pos;
    }
}
