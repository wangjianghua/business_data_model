<?php

namespace Framework\Foundation\Request;

trait ServerRequestExtend
{
    public function isAjax()
    {
        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]) == "xmlhttprequest") {
            return true;
        } else {
            if ($this->getIn('_ajax')) {
                return true;
            }
            return false;
        }
    }

    public function isJsonType()
    {
        $contentType = $this->getHeader("Content-Type");
        if (empty($contentType)) {
            return false;
        }

        if (false !== strpos($contentType[0], 'application/json')) {
            return true;
        }

        return false;
    }

    public function isGet()
    {
        return 'GET' == $_SERVER['REQUEST_METHOD'];
    }

    public function isPost()
    {
        return 'POST' == $_SERVER['REQUEST_METHOD'];
    }

    public function getAjaxMethod()
    {
        $ajaxMethod = $this->getIn('_ajax_method');

        return $ajaxMethod ?? $this->getMethod();
    }


    public function getByKey(string $key)
    {
        if (isset($this->parsedBody[$key])) {
            return $this->parsedBody[$key];
        }

        if (isset($this->queryParams[$key])) {
            return $this->queryParams[$key];
        }

        return null;
    }

    public function getIn($key, $default = null)
    {
        if (is_array($key)) {
            return $this->getInArray($key);
        }

        $ret = $this->getByKey($key);

        return is_null($ret) ? $default : $ret;
    }

    public function getInArray(array $keys, $default = null, $fillDefault = true): array
    {
        $data = [];
        foreach ($keys as $key) {
            $ret = $this->getByKey($key);
            if (!is_null($ret)) {
                $data[$key] = $ret;
                continue;
            }

            if ($fillDefault) {
                $data[$key] = $default;
            }
        }

        return $data;
    }

//    public function getUrl()
//    {
//        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//    }

//    public function getMethod()
//    {
//        return $_SERVER['REQUEST_METHOD'];
//    }

//    public function getClientIp()
//    {
//        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
//    }

//    public function getScheme()
//    {
//        return $_SERVER['REQUEST_SCHEME'];
//    }

//    public function getHost($withScheme = true)
//    {
//        if ($withScheme) {
//            return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
//        } else {
//            return $_SERVER['HTTP_HOST'];
//        }
//    }
//
//    public function getUser()
//    {
//        return $_SERVER['USER'];
//    }

//    public function getPath()
//    {
//        return $_SERVER['DOCUMENT_URI'];
//    }
//
//    public function getQuery()
//    {
//        return $_SERVER['QUERY_STRING'];
//    }

//    https://stackoverflow.com/questions/2317508/get-fragment-value-after-hash-from-a-url-in-php
//    public function getFragment()
//    {
//         获取不到， #后面的内容浏览器是不会发送给服务端的
//    }
}
