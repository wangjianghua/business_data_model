<?php

namespace Framework\Foundation;

/**
 * 数据仓库
 * Class Warehouse
 * @package Framework\Utils
 *
 */
class Warehouse
{
    //创建静态私有的变量保存该类对象
    protected static $instances = [];
    protected $scopeName = null;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @param string $scope
     * @param bool $initContainer
     * @return bool|self
     */
    public static function instance($scope = 'default', $initContainer = true)
    {
        if (!isset(self::$instances[$scope])) {
            if (!$initContainer) {
                return false;
            }
            self::$instances[$scope] = new self();
            self::$instances[$scope]->scopeName = $scope;
        }
        return self::$instances[$scope];
    }

    /**
     * @var $data \GuzzleHttp\Psr7\Response ['response']
     */
    private $data;

    public function set($key, $value)
    {
        if (isset($this->data[$key])) {
            return false;
        }
        $this->data[$key] = $value;
        return true;
    }

    public function setForce($key, $value)
    {
        $this->data[$key] = $value;
        return true;
    }

    public function get($key, $default = null)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return $default;
    }

    public function getScope()
    {
        return $this->scopeName;
    }

    public function __set($name, $value)
    {
        self::set($name, $value);
    }

    public function __get($name)
    {
        self::get($name);
    }
}
