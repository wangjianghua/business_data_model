<?php

namespace Framework\Foundation\Route;

use FastRoute\RouteCollector;

class RouterProcessor
{

    /**
     * @var RouteCollector
     */
    protected $routeCollector;

    public function __construct(RouteCollector $routeCollector)
    {
        $this->routeCollector = $routeCollector;
    }

    public function processRouteConf(array $config)
    {
        $commonMiddleware = $config['middleware'] ?? [];

        if (isset($config['groups'])) {
            $this->processGroupRoute($config['groups'], $commonMiddleware);
        }

        if (isset($config['routes'])) {
            $this->processRoute($config['routes'], $commonMiddleware);
        }
    }

//    public function processConfGroup(array $groupConf)
//    {
//        foreach ($groupConf as $oneGroup) {
//            $groupPrefix = $oneGroup['prefix'];
//            $groupPreMiddle = $oneGroup['middleware']['pre'] ?? [];
//            $groupPostMiddle = $oneGroup['middleware']['post'] ?? [];
//
//            foreach ($oneGroup['routes'] as $oneRoute) {
//                $preMiddle = $oneGroup['2']['preMiddleware'] ?? [];
//                $postMiddle = $oneGroup['2']['postMiddleware'] ?? [];
//
//                if (is_string($oneRoute['2'])) {
//                    $action['action'] = $oneRoute['2'];
//                } else {
//                    $action['action'] = $oneRoute['2']['action'];
//                }
//                $action['preMiddleware'] = array_merge($groupPreMiddle, $preMiddle);
//                $action['postMiddleware'] = array_merge($groupPostMiddle, $postMiddle);
//
//                $conf[0] = $oneRoute[0];
//                $conf[1] = '/' . $groupPrefix . $oneRoute[1];
//                $conf[2] = $action;
//
//                call_user_func_array([$this->routeCollector, 'addRoute'], $conf);
//            }
//        }
//    }

    public function processGroupRoute(array $groupConf, $commonMiddleware)
    {
        foreach ($groupConf as $oneGroup) {
            $groupPrefix = ltrim($oneGroup['prefix'], '/');
            $middleware = array_merge($commonMiddleware, $oneGroup['middleware']);

            foreach ($oneGroup['routes'] as $oneRoute) {
                if (isset($oneRoute['2']['middleware'])) {
                    $middle = array_merge($middleware, $oneRoute['2']['middleware']);
                } else {
                    $middle = $middleware;
                }

                $action = is_string($oneRoute['2']) ? $oneRoute['2'] : $oneRoute['2']['action'];

                $routeConf[0] = $oneRoute[0];
                $routeConf[1] = '/' . $groupPrefix . $oneRoute[1];
                $routeConf[2] = ['action' => $action, 'middleware' => $middle];

                call_user_func_array([$this->routeCollector, 'addRoute'], $routeConf);
            }
        }
    }


    public function processRoute(array $routeConf, $commonMiddleware)
    {
        foreach ($routeConf as $oneRouteConf) {
            if (is_string($oneRouteConf[2])) {
                $oneRouteConf[2] = [
                    'action' => $oneRouteConf[2],
                    'middleware' => $commonMiddleware
                ];
            } else {
                $oneRouteConf[2]['middleware'] = array_merge($commonMiddleware, $oneRouteConf[2]['middleware']);
            }
            call_user_func_array([$this->routeCollector, 'addRoute'], $oneRouteConf);
        }
    }
}
