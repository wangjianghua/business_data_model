<?php

namespace Framework\Foundation\Route;

use FastRoute\RouteCollector;
use FastRoute\Dispatcher\GroupCountBased;

class RouterMode2
{

    /**
     * @var RouteCollector
     */
    public static $routeCollector;


    public static function initRouteCollector($routeCollector = null)
    {
        if (!is_null($routeCollector)) {
            self::$routeCollector = $routeCollector;
            return;
        }

        $assistant = new RouterFactory();
        self::$routeCollector = $assistant->getRouteCollector();
    }

    public static function getDispatcher()
    {
        return new GroupCountBased(self::$routeCollector->getData());
    }

    /**
     * 路由前缀
     * @var
     */
    protected static $groupPrefix;

    /**
     * @var array
     * 配置说明：
     * 'middleware' 群组路由
     */
    public static $groupOptions = ['middleware' => []];

    public static function __callStatic($name, $arguments)
    {
        $router = static::$routeCollector;
        return $router->{$name}(...$arguments);
    }

    public static function group($prefix, callable $callback, $options = [])
    {
        $previousGroupPrefix = self::$groupPrefix;
        $previousGroupOptions = self::$groupOptions;

        self::$groupPrefix = $previousGroupPrefix . $prefix;
        self::$groupOptions = array_merge_recursive($previousGroupOptions, $options);
        $callback();
        self::$groupPrefix = $previousGroupPrefix;
        self::$groupOptions = $previousGroupOptions;
    }

    public static function getGroupPrefix()
    {
        return self::$groupPrefix;
    }

    public static function getGroupOptions()
    {
        return self::$groupOptions;
    }

    public static function addRoute($method, $route, $handler, $options = [])
    {
        $joinGroupRoute = self::getGroupPrefix() . $route;
        $mergeGroupOptions = array_merge_recursive(self::getGroupOptions(), $options);
        $mergeGroupOptions['action'] = $handler;

        self::$routeCollector->addRoute($method, $joinGroupRoute, $mergeGroupOptions);
    }

    public static function get($route, $handler, $options = [])
    {
        self::addRoute('GET', $route, $handler, $options);
    }

    public static function post($route, $handler, $options = [])
    {
        self::addRoute('POST', $route, $handler, $options);
    }

    public static function put($route, $handler, $options = [])
    {
        self::addRoute('PUT', $route, $handler, $options);
    }

    public static function delete($route, $handler, $options = [])
    {
        self::addRoute('DELETE', $route, $handler, $options);
    }

    public static function patch($route, $handler, $options = [])
    {
        self::addRoute('PATCH', $route, $handler, $options);
    }

    public static function head($route, $handler, $options = [])
    {
        self::addRoute('HEAD', $route, $handler, $options);
    }

    public static function any($route, $handler, $options = [])
    {
        self::addRoute(['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD'], $route, $handler, $options);
    }

    public static function gpp($route, $handler, $options = [])
    {
        self::addRoute(['GET', 'POST', 'PUT'], $route, $handler, $options);
    }

    public static function rest($route, $handler, $options = [])
    {
        self::addRoute(['GET', 'POST', 'PUT', 'DELETE'], $route, $handler, $options);
    }


    public static function getRouteCollector()
    {
        return self::$routeCollector;
    }
}
