<?php

namespace Framework\Foundation\Route;

use FastRoute\DataGenerator;
use FastRoute\RouteParser;
use FastRoute\RouteCollector;
use FastRoute\Dispatcher\GroupCountBased;

/**
 * 框架router工厂方法， 不再多余加一个接口
 * 方法getRouteCollector 返回路由收集器
 * 方法getRouteDispatcher 返回路由匹配器
 *
 * Class RouterAssistant
 * @package Framework\Foundation\Route
 */
class RouterFactory
{

    /**
     * 获得默认RouteCollector
     * 默认RouteCollector可以简单理解是一个DataGenerator的包装
     * @param RouteParser|null $routeParser
     * @param DataGenerator|null $dataGenerator
     * @return RouteCollector
     */
    public function getRouteCollector(RouteParser $routeParser = null, DataGenerator $dataGenerator = null)
    {
        if (is_null($routeParser)) {
            $routeParser = new RouteParser\Std();
        }
        if (is_null($dataGenerator)) {
            $dataGenerator = new DataGenerator\GroupCountBased();
        }

        return new RouteCollector($routeParser, $dataGenerator);
    }

    /**
     * RouteCollector注册路由完成后， 交给Dispatcher，由dispatcher检验请求的uri是否允许。
     * @param array $routeData
     * @return GroupCountBased
     */
    public function getRouteDispatcher($routeData)
    {
        return new GroupCountBased($routeData);
    }
}
