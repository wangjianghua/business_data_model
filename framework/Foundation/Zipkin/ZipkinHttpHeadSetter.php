<?php

namespace Framework\Foundation\Zipkin;

use Zipkin\Propagation\Setter;

class ZipkinHttpHeadSetter implements Setter
{
    const TRACE_ID_NAME = 'X-B3-TraceId';
    const SPAN_ID_NAME = 'X-B3-SpanId';
    const PARENT_SPAN_ID_NAME = 'X-B3-ParentSpanId';
    const SAMPLED_NAME = 'X-B3-Sampled';
    const FLAGS_NAME = 'X-B3-Flags';

    public $headers = [
        self::TRACE_ID_NAME => 'o-default',
        self::SPAN_ID_NAME => 'o-default',
        self::PARENT_SPAN_ID_NAME => '',
        self::SAMPLED_NAME => '1',
        self::FLAGS_NAME => null,
    ];

    public function put(&$carrier, $key, $value)
    {
        $carrier[$key] = $value;
    }
}
