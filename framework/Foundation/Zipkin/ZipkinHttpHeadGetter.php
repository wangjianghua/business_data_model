<?php

namespace Framework\Foundation\Zipkin;

use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Zipkin\Propagation\Getter;

class ZipkinHttpHeadGetter implements Getter
{
    const TRACE_ID_NAME = 'X-B3-TraceId';
    const SPAN_ID_NAME = 'X-B3-SpanId';
    const PARENT_SPAN_ID_NAME = 'X-B3-ParentSpanId';
    const SAMPLED_NAME = 'X-B3-Sampled';
    const FLAGS_NAME = 'X-B3-Flags';

    public $headers = [
        self::TRACE_ID_NAME => 'o-default',
        self::SPAN_ID_NAME => 'o-default',
        self::PARENT_SPAN_ID_NAME => '',
        self::SAMPLED_NAME => '1',
        self::FLAGS_NAME => null,
    ];

    /**
     * @param ServerRequestInterface $carrier 暂时用不到，载体? header? get？ redis? post?
     * @param string $key
     * @return mixed|null|string
     */
    public function get($carrier, $key)
    {
        if ($carrier instanceof ServerRequest) {
            $ret = $carrier->getHeader($key);
            return empty($ret) ? '' : $ret[0];
        }
    }
}
