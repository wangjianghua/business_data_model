<?php

namespace Framework\Foundation\Zipkin;

use Framework\Foundation\Request\ServerRequestProxy;
use Zipkin\Endpoint;
use Zipkin\Reporter;
use Zipkin\Samplers\BinarySampler;
use Zipkin\Reporters\Http;
use Zipkin\Span;
use Zipkin\Tracer;
use Zipkin\TracingBuilder;
use Zipkin\DefaultTracing;

class ZipkinHelper
{
    //创建静态私有的变量保存该类对象
    protected static $instance;

    public $config = [
        'reporter' => [
            'endpoint_url' => 'http://host.docker.internal:9411/api/v2/spans'
        ],

        'tracing' => [
        ]
    ];

    public $defaultReporter;

    /**
     * @var DefaultTracing;
     */
    protected $tracing;

    /**
     * @var Tracer;
     */
    public $tracer;

    /**
     * @var Span;
     */
    protected $tempCurrentSpan;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function instance($config = [], Reporter $reporter = null)
    {
        if (!self::$instance instanceof self) {
            $helper = new self();
            $helper->config = array_merge($helper->config, $config);

            if (is_null($reporter)) {
                $helper->defaultReporter = new Http(null, $helper->config['reporter']);
            } else {
                $helper->defaultReporter = $reporter;
            }

            $helper->build();
            self::$instance = $helper;
        }

        return self::$instance;
    }

    public function build($serviceName = 'default')
    {
        $ip4 = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : null;
        $ip6 = null;
        $port = array_key_exists('REMOTE_PORT', $_SERVER) ? (int)$_SERVER['REMOTE_PORT'] : null;

        $ip4 = $this->config['tracing']['ip4'] ?? $ip4;
        $ip6 = $this->config['tracing']['ip6'] ?? $ip6;
        $port = $this->config['tracing']['port'] ?? $port;

        $endpoint = Endpoint::create($serviceName, $ip4, $ip6, $port);
        $sampler = BinarySampler::createAsAlwaysSample();

        $this->tracing = TracingBuilder::create()
            ->havingLocalEndpoint($endpoint)
            ->havingSampler($sampler)
            ->havingReporter($this->defaultReporter)
            ->build();

        $this->tracer = $this->tracing->getTracer();
    }

    public function autoGetSpan()
    {
        // 实例中tempCurrentSpan不为null代表是应用内连续获取span
        if (!is_null($this->tempCurrentSpan)) {
            $this->tempCurrentSpan = $this->tracer->nextSpan($this->tempCurrentSpan->getContext());
            return $this->tempCurrentSpan;
        }

        // 从header获得传播span
        $propagationContext = $this->propagationContext();
        if ($propagationContext instanceof \Zipkin\Propagation\TraceContext) {
            $this->tempCurrentSpan = $this->tracer->nextSpan($propagationContext);
            return $this->tempCurrentSpan;
        }

        // 生成一个根span返回
        $this->tempCurrentSpan = $this->rootSpan();
        return $this->tempCurrentSpan;
    }

    public function getNewSpan(Span $span = null)
    {
        $parentSpan = $span ?? $this->tracer->getCurrentSpan();
        return $this->tracer->nextSpan($parentSpan);
    }

    public function propagationContext()
    {
        $extractor = $this->tracing->getPropagation()->getExtractor(new ZipkinHttpHeadGetter());
        $extractedContext = $extractor(ServerRequestProxy::fromGlobalsAndHandleJson()->getServerRequest());
        return $extractedContext;
    }

    public function rootSpan()
    {
        return $this->tracer->newTrace();
    }

    public function getTracing()
    {
        return $this->tracing;
    }

    public function getTracer()
    {
        return $this->tracer;
    }

    public function setConfig($config = [])
    {
        $this->config = array_merge($this->config, $config);
    }
}
