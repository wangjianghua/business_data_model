<?php

namespace Framework;

use FastRoute\Dispatcher;
use SebastianBergmann\FileIterator\Facade;
use Framework\Foundation\Route\RouterProcessor;
use Framework\Foundation\Route\RouterHelper;

/**
 * Trait FrameRoutePart
 * @package Framework
 */
trait FrameRoutePart
{
    public function routeInitAndDispatch($routeInfo)
    {
        /**
         * @var $dispatcher Dispatcher;
         */
        $dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r) {
            // 加载routeConf 解析 注册
            $routeProcessor = new RouterProcessor($r);
            $routeFiles = (new Facade())->getFilesAsArray(ROOT . "/app/config/routes", 'php');
            foreach ($routeFiles as $routeFile) {
                $conf = include $routeFile;
                $routeProcessor->processRouteConf($conf);
            }
        });

        $dispatchInfo = $dispatcher->dispatch($routeInfo['method'], $routeInfo['route']);
        return $dispatchInfo;
    }

    public function restFulRouteInitAndDispatch($restFulRouteInfo)
    {
        /**
         * @var $dispatcher Dispatcher;
         */
        $dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r) {
            // 加载routeConf 解析 注册
            $routeProcessor = new RouterProcessor($r);
            $routeFiles = (new Facade())->getFilesAsArray(ROOT . "/app/config/rest/routes", 'php');
            foreach ($routeFiles as $routeFile) {
                $conf = include $routeFile;
                $routeProcessor->processRouteConf($conf);
            }
        });

        $restDispatchInfo = $dispatcher->dispatch($restFulRouteInfo['method'], $restFulRouteInfo['route']);
        return $restDispatchInfo;
    }
}
