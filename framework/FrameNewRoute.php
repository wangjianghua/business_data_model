<?php

namespace Framework;

use FastRoute\Dispatcher;
use SebastianBergmann\FileIterator\Facade;
use Framework\Foundation\Route\Router;

trait FrameNewRoute
{
    public function routeInitAndDispatch($routeInfo)
    {
        Router::initRouteCollector();

        $routeFiles = (new Facade())->getFilesAsArray(ROOT . "/app/config/route/controller/", 'php');
        if (empty($routeFiles)) {
            return [
                0 => Dispatcher::NOT_FOUND,
            ];
        }
        foreach ($routeFiles as $routeFile) {
            include $routeFile;
        }

        return Router::getDispatcher()->dispatch($routeInfo['method'], $routeInfo['route']);
    }

    public function restFulRouteInitAndDispatch($restFulRouteInfo)
    {
        Router::initRouteCollector();
        $routeFiles = (new Facade())->getFilesAsArray(ROOT . "/app/config/route/rest/", 'php');
        if (empty($routeFiles)) {
            return [
                0 => Dispatcher::NOT_FOUND,
            ];
        }
        foreach ($routeFiles as $routeFile) {
            include $routeFile;
        }

        return Router::getDispatcher()->dispatch($restFulRouteInfo['method'], $restFulRouteInfo['route']);
    }
}
