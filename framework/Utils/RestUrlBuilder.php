<?php


namespace Framework\Utils;

class RestUrlBuilder
{
    public static function genGetListUrl($resource, array $queryParams = [])
    {
    }

    public static function genGetInfoUrl($resource, $id, array $queryParams = [])
    {
    }

    public static function genAddUrl($resource, array $queryParams = [])
    {
    }

    public static function genEditUrl($resource, $id, array $queryParams = [])
    {
    }

    public static function genDeleteUrl($resource, $id, array $queryParams = [])
    {
    }

    public static function genUrl()
    {
    }
}
