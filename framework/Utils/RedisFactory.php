<?php

namespace Framework\Utils;

use Framework\Foundation\Config;

class RedisFactory
{
    public static $handlers = [];

    public static function createRaw($host = null, $port = null, $auth = null)
    {
        $host = $host ?? Config::instance()->get('database.redis.default.host');
        $port = $port ?? Config::instance()->get('database.redis.default.port');

        $redis = new \Redis();
        $redis->connect($host, $port);
        if ($auth) {
            $redis->auth($auth);
        }

        return $redis;
    }

    public static function create($name = 'default', $new = false)
    {
        $host = Config::instance()->get('database.redis.' . $name . '.host');
        $port = Config::instance()->get('database.redis.' . $name . '.port');
        $password = Config::instance()->get('database.redis.' . $name . '.password');

        $redis = new \Redis();
        $redis->connect($host, $port);
        if ($password) {
            $redis->auth($password);
        }

        return $redis;
    }
}
