<?php

namespace Framework\Exceptions;

use Exception;

/**
 * @see http://php.net/manual/zh/spl.exceptions.php [php自带标准异常]
 * @see http://noodle.exwechat.com/source/exception.html
 */
class FrameException extends Exception
{
    protected $details;

    public function __construct(string $message = '', int $code = 1, $details = [], Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->details = $details;
    }

    /**
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

    public function resetCode($code)
    {
        $this->code = $code;
    }

    public function resetMessage($message)
    {
        $this->message = $message;
    }

    public function resetDetails($details)
    {
        $this->details = $details;
    }
}
