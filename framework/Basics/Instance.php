<?php

namespace Framework\Basics;

class Instance
{

    //创建静态私有的变量保存该类对象
    protected static $instance;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
