/*
 Navicat Premium Data Transfer

 Source Server         : myali-rds
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : rm-2zemxuvee9kii2b55so.mysql.rds.aliyuncs.com:3306
 Source Schema         : business_event

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 16/11/2020 17:16:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `administrator_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缩略图',
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`administrator_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='后台管理人员';

-- ----------------------------
-- Records of administrator
-- ----------------------------
BEGIN;
INSERT INTO `administrator` VALUES (1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '主管理帐号', NULL, 33, '2019-05-23 12:09:26', '2020-07-21 22:47:03', 0, '2020-07-20 14:53:30');
INSERT INTO `administrator` VALUES (3, 'owen', '8cb2237d0679ca88db6464eac60da96345513964', 'show me', NULL, 1, '2019-05-23 12:09:26', '2020-07-22 10:21:48', 0, '2020-07-20 14:53:18');
INSERT INTO `administrator` VALUES (6, 'test', NULL, 'aaaaa', NULL, 1, NULL, '2020-09-20 10:12:18', 1, '2020-09-20 02:12:18');
INSERT INTO `administrator` VALUES (7, 'www', NULL, 'wwwwqqaass', NULL, 127, '2020-07-20 22:23:13', '2020-07-21 21:16:37', 0, '2020-07-20 14:52:45');
COMMIT;

-- ----------------------------
-- Table structure for ddd_event
-- ----------------------------
DROP TABLE IF EXISTS `ddd_event`;
CREATE TABLE `ddd_event` (
  `ddd_event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'mysql-update',
  `event_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件标签',
  `event_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件名称',
  `stream_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件包含的mysql操作',
  `event_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `event_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '事件外部文档',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '事件说明',
  `is_deleted` int(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ddd_event_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of ddd_event
-- ----------------------------
BEGIN;
INSERT INTO `ddd_event` VALUES (14, 'mysql-update', '', '事件A', '5758,5734', '', '', NULL, 0, NULL, '2020-11-15 22:55:50', '2020-11-16 17:11:09');
INSERT INTO `ddd_event` VALUES (15, 'mysql-update', '', '事件B', '5758,5733', '', '', NULL, 0, NULL, '2020-11-15 22:57:47', '2020-11-16 17:11:10');
INSERT INTO `ddd_event` VALUES (16, 'mysql-update', '', '支付完成事件', '6787,6786,5793', 'v20201116-01', '', NULL, 0, NULL, '2020-11-16 12:52:14', '2020-11-16 17:15:57');
COMMIT;

-- ----------------------------
-- Table structure for ddd_event_stream
-- ----------------------------
DROP TABLE IF EXISTS `ddd_event_stream`;
CREATE TABLE `ddd_event_stream` (
  `ddd_event_stream_id` int(11) NOT NULL AUTO_INCREMENT,
  `db_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transaction_tag` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `event_type` int(11) DEFAULT '-100',
  `columns` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `update_columns` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '更新的字段',
  `update_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '更新字段的值',
  `ignore_column_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '忽略的字段值',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ddd_event_stream_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8008 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of ddd_event_stream
-- ----------------------------
BEGIN;
INSERT INTO `ddd_event_stream` VALUES (5714, 'contra_test_rw', 'contract', '5fadf6dd9413b-2020-11-13 11:00:45', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"3\",\"updated_at\":\"2020-11-13 11:00:45\",\"sys_update_dc\":\"2020-11-13 11:00:45\"}', NULL, NULL, 0, NULL, '2020-11-13 11:00:45', '2020-11-13 11:00:45');
INSERT INTO `ddd_event_stream` VALUES (5727, 'eas_test', 'classes', '5fae5c13c776c-2020-11-13 18:12:35', 2, 'id,uid,campus_id,teaching_center_id,product_id,classroom_id,classroom_ids,time_template_id,name,dict_class_status_id,plan_min_class_number,plan_max_class_number,insert_class,promoted_status,coursed,sold_degrees,class_teacher_master_name,guidance_teacher,start_sale_time,start_attend_class_time,end_class_time,inside_sell_time,external_sell_time,is_have_promoted,is_original,is_short_term,is_online,is_demo,is_make_up,is_experiment,is_demo_class,demo_class_category_id,set_online_status,old_class_id,half_start_date,deleted,deleted_at,created_at,updated_at,sys_update_dc', 'id,uid,campus_id,teaching_center_id,product_id,classroom_id,classroom_ids,time_template_id,name,dict_class_status_id,plan_min_class_number,plan_max_class_number,insert_class,promoted_status,coursed,sold_degrees,class_teacher_master_name,guidance_teacher,start_sale_time,start_attend_class_time,end_class_time,inside_sell_time,external_sell_time,is_have_promoted,is_original,is_short_term,is_online,is_demo,is_make_up,is_experiment,is_demo_class,demo_class_category_id,set_online_status,old_class_id,half_start_date,deleted,deleted_at,created_at,updated_at,sys_update_dc', '{\"id\":\"10010637\",\"uid\":\"20\",\"campus_id\":\"37\",\"teaching_center_id\":\"116\",\"product_id\":\"263\",\"classroom_id\":\"0\",\"classroom_ids\":\"2061,2528,2304,2302,0\",\"time_template_id\":\"56\",\"name\":\"升级班解绑_TEST\",\"dict_class_status_id\":\"3\",\"plan_min_class_number\":\"12\",\"plan_max_class_number\":\"14\",\"insert_class\":\"0\",\"promoted_status\":\"1\",\"coursed\":\"0\",\"sold_degrees\":\"0\",\"class_teacher_master_name\":\"\",\"guidance_teacher\":\"\",\"start_sale_time\":\"\",\"start_attend_class_time\":\"2021-01-17 00:00:00\",\"end_class_time\":\"\",\"inside_sell_time\":\"2020-12-12 11:30:00\",\"external_sell_time\":\"2020-12-26 12:10:00\",\"is_have_promoted\":\"0\",\"is_original\":\"1\",\"is_short_term\":\"0\",\"is_online\":\"-1\",\"is_demo\":\"0\",\"is_make_up\":\"0\",\"is_experiment\":\"0\",\"is_demo_class\":\"0\",\"demo_class_category_id\":\"0\",\"set_online_status\":\"0\",\"old_class_id\":\"\",\"half_start_date\":\"\",\"deleted\":\"0\",\"deleted_at\":\"\",\"created_at\":\"2020-11-13 15:06:47\",\"updated_at\":\"2020-11-13 15:06:47\",\"sys_update_dc\":\"2020-11-13 15:06:47\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:35', '2020-11-13 18:12:35');
INSERT INTO `ddd_event_stream` VALUES (5728, 'eas_test', 'classes', '5fae5c13c776c-2020-11-13 18:12:35', 2, 'id,uid,campus_id,teaching_center_id,product_id,classroom_id,classroom_ids,time_template_id,name,dict_class_status_id,plan_min_class_number,plan_max_class_number,insert_class,promoted_status,coursed,sold_degrees,class_teacher_master_name,guidance_teacher,start_sale_time,start_attend_class_time,end_class_time,inside_sell_time,external_sell_time,is_have_promoted,is_original,is_short_term,is_online,is_demo,is_make_up,is_experiment,is_demo_class,demo_class_category_id,set_online_status,old_class_id,half_start_date,deleted,deleted_at,created_at,updated_at,sys_update_dc', 'is_have_promoted,updated_at,sys_update_dc', '{\"is_have_promoted\":\"1\",\"updated_at\":\"2020-11-13 15:06:47\",\"sys_update_dc\":\"2020-11-13 15:06:47\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:35', '2020-11-13 18:12:35');
INSERT INTO `ddd_event_stream` VALUES (5732, 'contra_test_rw', 'contract', '5fae5c13eb81e-2020-11-13 18:12:35', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,real_amount,updated_at,sys_update_dc', '{\"status\":\"3\",\"real_amount\":\"41500\",\"updated_at\":\"2020-11-13 15:11:20\",\"sys_update_dc\":\"2020-11-13 15:11:20\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:36', '2020-11-13 18:12:36');
INSERT INTO `ddd_event_stream` VALUES (5733, 'contra_test_rw', 'contract', '5fae5c140737a-2020-11-13 18:12:36', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', '{\"id\":\"262601774476851584\",\"type\":\"1\",\"status\":\"1\",\"parent_id\":\"2548446081616826116\",\"sign_time\":\"2020-11-13 15:25:41\",\"student_id\":\"14510198323002218279\",\"student_no\":\"S2010121004092\",\"campus_area_id\":\"37\",\"campus_id\":\"116\",\"product_id\":\"354\",\"product_name\":\"K2A\",\"class_num\":\"22\",\"standard_amount\":\"45650\",\"real_amount\":\"0\",\"total_amount\":\"45650\",\"finance_id\":\"0\",\"tax_id\":\"141\",\"related_id\":\"0\",\"not_divide_num\":\"22\",\"is_history\":\"0\",\"begin_serial_id\":\"0\",\"end_serial_id\":\"0\",\"class_serial_ids\":\"19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40\",\"exec_campus_id\":\"116\",\"parent_phone\":\"1rTzpJRCCgW4VBLvQZVkBA==\",\"pay_class_num\":\"2\",\"pay_serial_ids\":\"19,20\",\"step_serial_ids\":\"{\\\"1\\\":[\\\"19\\\",\\\"20\\\"]}\",\"end_time\":\"\",\"encrypt\":\"0\",\"origin_campus_area_id\":\"37\",\"origin_campus_id\":\"116\",\"created_at\":\"2020-11-13 15:25:42\",\"updated_at\":\"2020-11-13 15:25:42\",\"deleted_at\":\"\",\"sys_update_dc\":\"2020-11-13 15:25:42\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:36', '2020-11-13 18:12:36');
INSERT INTO `ddd_event_stream` VALUES (5734, 'contra_test_rw', 'contract', '5fae5c140c0bd-2020-11-13 18:12:36', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,real_amount', '{\"status\":\"3\",\"real_amount\":\"4150\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:36', '2020-11-13 18:12:36');
INSERT INTO `ddd_event_stream` VALUES (5758, 'eas_test', 'class_teachers', '5fae5c16f0618-2020-11-13 18:12:38', 2, 'id,class_id,teacher_id,dict_teacher_duty_id,dict_teacher_attr_id,teacher_sort,deleted,deleted_at,created_at,updated_at,sys_update_dc', 'id,class_id,teacher_id,dict_teacher_duty_id,dict_teacher_attr_id,teacher_sort,deleted,deleted_at,created_at,updated_at,sys_update_dc', '{\"id\":\"89568\",\"class_id\":\"10010643\",\"teacher_id\":\"8580\",\"dict_teacher_duty_id\":\"2\",\"dict_teacher_attr_id\":\"1\",\"teacher_sort\":\"1\",\"deleted\":\"0\",\"deleted_at\":\"\",\"created_at\":\"2020-11-13 17:09:27\",\"updated_at\":\"\",\"sys_update_dc\":\"2020-11-13 17:09:27\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:39', '2020-11-13 18:12:39');
INSERT INTO `ddd_event_stream` VALUES (5759, 'eas_test', 'class_teachers', '5fae5c17074f1-2020-11-13 18:12:39', 2, 'id,class_id,teacher_id,dict_teacher_duty_id,dict_teacher_attr_id,teacher_sort,deleted,deleted_at,created_at,updated_at,sys_update_dc', 'deleted,deleted_at,updated_at,sys_update_dc', '{\"deleted\":\"1\",\"deleted_at\":\"2020-11-13 17:13:23\",\"updated_at\":\"2020-11-13 17:13:23\",\"sys_update_dc\":\"2020-11-13 17:13:23\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:39', '2020-11-13 18:12:39');
INSERT INTO `ddd_event_stream` VALUES (5767, 'eas_test', 'classes', '5fae5c1779c5a-2020-11-13 18:12:39', 2, 'id,uid,campus_id,teaching_center_id,product_id,classroom_id,classroom_ids,time_template_id,name,dict_class_status_id,plan_min_class_number,plan_max_class_number,insert_class,promoted_status,coursed,sold_degrees,class_teacher_master_name,guidance_teacher,start_sale_time,start_attend_class_time,end_class_time,inside_sell_time,external_sell_time,is_have_promoted,is_original,is_short_term,is_online,is_demo,is_make_up,is_experiment,is_demo_class,demo_class_category_id,set_online_status,old_class_id,half_start_date,deleted,deleted_at,created_at,updated_at,sys_update_dc', 'start_attend_class_time,end_class_time,updated_at,sys_update_dc', '{\"start_attend_class_time\":\"2020-11-13 17:00:00\",\"end_class_time\":\"2020-12-20 14:50:00\",\"updated_at\":\"2020-11-13 17:21:56\",\"sys_update_dc\":\"2020-11-13 17:21:56\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:39', '2020-11-13 18:12:39');
INSERT INTO `ddd_event_stream` VALUES (5768, 'contra_test_rw', 'contract', '5fae5c1781c3f-2020-11-13 18:12:39', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', '{\"id\":\"262631759946215552\",\"type\":\"1\",\"status\":\"1\",\"parent_id\":\"2548446081616707826\",\"sign_time\":\"2020-11-13 17:24:50\",\"student_id\":\"2548446081616708255\",\"student_no\":\"S2001071000127\",\"campus_area_id\":\"37\",\"campus_id\":\"116\",\"product_id\":\"585\",\"product_name\":\"G1-1线下\",\"class_num\":\"24\",\"standard_amount\":\"100000\",\"real_amount\":\"0\",\"total_amount\":\"100000\",\"finance_id\":\"0\",\"tax_id\":\"141\",\"related_id\":\"0\",\"not_divide_num\":\"24\",\"is_history\":\"0\",\"begin_serial_id\":\"0\",\"end_serial_id\":\"0\",\"class_serial_ids\":\"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24\",\"exec_campus_id\":\"116\",\"parent_phone\":\"tWsFVk5Lb5LU8zF0aYmpqA==\",\"pay_class_num\":\"24\",\"pay_serial_ids\":\"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24\",\"step_serial_ids\":\"{\\\"1\\\":[\\\"1\\\",\\\"2\\\",\\\"3\\\",\\\"4\\\",\\\"5\\\",\\\"6\\\",\\\"7\\\",\\\"8\\\",\\\"9\\\",\\\"10\\\",\\\"11\\\",\\\"12\\\",\\\"13\\\",\\\"14\\\",\\\"15\\\",\\\"16\\\",\\\"17\\\",\\\"18\\\",\\\"19\\\",\\\"20\\\",\\\"21\\\",\\\"22\\\",\\\"23\\\",\\\"24\\\"]}\",\"end_time\":\"\",\"encrypt\":\"0\",\"origin_campus_area_id\":\"37\",\"origin_campus_id\":\"116\",\"created_at\":\"2020-11-13 17:24:51\",\"updated_at\":\"2020-11-13 17:24:51\",\"deleted_at\":\"\",\"sys_update_dc\":\"2020-11-13 17:24:51\"}', NULL, NULL, 0, NULL, '2020-11-13 18:12:39', '2020-11-13 18:12:39');
INSERT INTO `ddd_event_stream` VALUES (5785, 'mysql', 'ha_health_check', '5fb1efa7578f5-1605496743', 2, 'id,type', 'id', '{\"id\":\"1605496742335\"}', NULL, NULL, 0, NULL, '2020-11-16 11:19:04', '2020-11-16 11:19:04');
INSERT INTO `ddd_event_stream` VALUES (5786, 'mysql', 'ha_health_check', '5fb1efb5e8994-1605496757', 2, 'id,type', 'id', '{\"id\":\"1605496757185\"}', NULL, NULL, 0, NULL, '2020-11-16 11:19:17', '2020-11-16 11:19:17');
INSERT INTO `ddd_event_stream` VALUES (5787, 'contra_test_rw', 'contract', '5fb1efba45ae6-1605496762', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"2\",\"updated_at\":\"2020-11-16 11:19:21\",\"sys_update_dc\":\"2020-11-16 11:19:21\"}', NULL, NULL, 0, NULL, '2020-11-16 11:19:22', '2020-11-16 11:19:22');
INSERT INTO `ddd_event_stream` VALUES (5788, 'mysql', 'ha_health_check', '5fb1efc55f47f-1605496773', 2, 'id,type', 'id', '{\"id\":\"1605496772285\"}', NULL, NULL, 0, NULL, '2020-11-16 11:19:33', '2020-11-16 11:19:33');
INSERT INTO `ddd_event_stream` VALUES (5789, 'mysql', 'ha_health_check', '5fb1efd362a64-1605496787', 2, 'id,type', 'id', '{\"id\":\"1605496787243\"}', NULL, NULL, 0, NULL, '2020-11-16 11:19:47', '2020-11-16 11:19:47');
INSERT INTO `ddd_event_stream` VALUES (5790, 'contra_test_rw', 'contract', '5fb1efe28a974-1605496802', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"4\",\"updated_at\":\"2020-11-16 11:20:02\",\"sys_update_dc\":\"2020-11-16 11:20:02\"}', NULL, NULL, 0, NULL, '2020-11-16 11:20:02', '2020-11-16 11:20:02');
INSERT INTO `ddd_event_stream` VALUES (5791, 'contra_test_rw', 'contract', '5fb1efe28a974-1605496802', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"4\",\"updated_at\":\"2020-11-16 11:20:02\",\"sys_update_dc\":\"2020-11-16 11:20:02\"}', NULL, NULL, 0, NULL, '2020-11-16 11:20:02', '2020-11-16 11:20:02');
INSERT INTO `ddd_event_stream` VALUES (5792, 'contra_test_rw', 'contract', '5fb1efe28a974-1605496802', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"4\",\"updated_at\":\"2020-11-16 11:20:02\",\"sys_update_dc\":\"2020-11-16 11:20:02\"}', NULL, NULL, 0, NULL, '2020-11-16 11:20:02', '2020-11-16 11:20:02');
INSERT INTO `ddd_event_stream` VALUES (5793, 'contra_test_rw', 'contract', '5fb1efe298023-1605496802', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'status,updated_at,sys_update_dc', '{\"status\":\"4\",\"updated_at\":\"2020-11-16 11:20:02\",\"sys_update_dc\":\"2020-11-16 11:20:02\"}', NULL, NULL, 0, NULL, '2020-11-16 11:20:02', '2020-11-16 11:20:02');
INSERT INTO `ddd_event_stream` VALUES (6786, 'contra_test_rw', 'contract', '', 2, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', '{\"id\":\"111111116\",\"type\":\"0\",\"status\":\"0\",\"parent_id\":\"0\",\"sign_time\":\"0000-00-00 00:00:00\",\"student_id\":\"0\",\"student_no\":\"\",\"campus_area_id\":\"0\",\"campus_id\":\"0\",\"product_id\":\"0\",\"product_name\":\"\",\"class_num\":\"0\",\"standard_amount\":\"0\",\"real_amount\":\"0\",\"total_amount\":\"0\",\"finance_id\":\"0\",\"tax_id\":\"0\",\"related_id\":\"0\",\"not_divide_num\":\"0\",\"is_history\":\"0\",\"begin_serial_id\":\"0\",\"end_serial_id\":\"0\",\"class_serial_ids\":\"\",\"exec_campus_id\":\"0\",\"parent_phone\":\"\",\"pay_class_num\":\"0\",\"pay_serial_ids\":\"\",\"step_serial_ids\":\"\",\"end_time\":\"\",\"encrypt\":\"0\",\"origin_campus_area_id\":\"0\",\"origin_campus_id\":\"0\",\"created_at\":\"2020-11-16 12:44:13\",\"updated_at\":\"2020-11-16 12:44:13\",\"deleted_at\":\"\",\"sys_update_dc\":\"2020-11-16 12:44:13\"}', NULL, NULL, 0, NULL, '2020-11-16 12:44:13', '2020-11-16 12:44:13');
INSERT INTO `ddd_event_stream` VALUES (6787, 'contra_test_rw', 'contract', '5fb2042e1614e-1605501998', 1, 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', 'id,type,status,parent_id,sign_time,student_id,student_no,campus_area_id,campus_id,product_id,product_name,class_num,standard_amount,real_amount,total_amount,finance_id,tax_id,related_id,not_divide_num,is_history,begin_serial_id,end_serial_id,class_serial_ids,exec_campus_id,parent_phone,pay_class_num,pay_serial_ids,step_serial_ids,end_time,encrypt,origin_campus_area_id,origin_campus_id,created_at,updated_at,deleted_at,sys_update_dc', '{\"id\":\"111111117\",\"type\":\"0\",\"status\":\"0\",\"parent_id\":\"0\",\"sign_time\":\"0000-00-00 00:00:00\",\"student_id\":\"0\",\"student_no\":\"\",\"campus_area_id\":\"0\",\"campus_id\":\"0\",\"product_id\":\"0\",\"product_name\":\"\",\"class_num\":\"0\",\"standard_amount\":\"0\",\"real_amount\":\"0\",\"total_amount\":\"0\",\"finance_id\":\"0\",\"tax_id\":\"0\",\"related_id\":\"0\",\"not_divide_num\":\"0\",\"is_history\":\"0\",\"begin_serial_id\":\"0\",\"end_serial_id\":\"0\",\"class_serial_ids\":\"\",\"exec_campus_id\":\"0\",\"parent_phone\":\"\",\"pay_class_num\":\"0\",\"pay_serial_ids\":\"\",\"step_serial_ids\":\"\",\"end_time\":\"\",\"encrypt\":\"0\",\"origin_campus_area_id\":\"0\",\"origin_campus_id\":\"0\",\"created_at\":\"2020-11-16 12:46:37\",\"updated_at\":\"2020-11-16 12:46:37\",\"deleted_at\":\"\",\"sys_update_dc\":\"2020-11-16 12:46:37\"}', NULL, NULL, 0, NULL, '2020-11-16 12:46:38', '2020-11-16 12:46:38');
INSERT INTO `ddd_event_stream` VALUES (6788, 'user_center_test', 'wx_relations', '5fb22429bbbf2-1605510185', 2, '', '', '[]', NULL, NULL, 0, NULL, '2020-11-16 15:03:05', '2020-11-16 15:03:05');
INSERT INTO `ddd_event_stream` VALUES (6789, 'user_center_test', 'wx_relations', '5fb22429bbbf2-1605510185', 1, 'id,parent_id,nick_name,head_pic,open_id,union_id,app_id,created_at,updated_at', 'id,parent_id,nick_name,head_pic,open_id,union_id,app_id,created_at,updated_at', '{\"id\":\"40003\",\"parent_id\":\"14510198323002067292\",\"nick_name\":\"チェリー\",\"head_pic\":\"https:\\/\\/thirdwx.qlogo.cn\\/mmopen\\/vi_32\\/Q0j4TwGTfTKVwJ30cBicvMr1GgUAWLHj61HTbk7GMB7PkcPq3DGze0MOyFpIwMYOBrGibX6u1SBIgOjoUupZqoyg\\/132\",\"open_id\":\"og-_D5ObmsCbyTE3w62G7ZYs16aY\",\"union_id\":\"oXlAds_bW5oR8VJDqIMRZicpRFMI\",\"app_id\":\"wx45b48d94f20d6400\",\"created_at\":\"2020-11-16 14:00:34\",\"updated_at\":\"2020-11-16 14:00:34\"}', NULL, NULL, 0, NULL, '2020-11-16 15:03:05', '2020-11-16 15:03:05');
INSERT INTO `ddd_event_stream` VALUES (6790, 'user_center_test', 'transfer', '5fb22429dee4d-1605510185', 1, 'transfer_id,contract_id,transfer_contract_id,student_id,parent_id,relationship_id,product_id,product_name,transfer_out_id,transfer_out_name,transfer_in_id,transfer_in_name,transfer_out_teaching_center_id,transfer_out_teaching_center,transfer_in_teaching_center_id,transfer_in_teaching_center,status,reason,transfer_time,approval_time,class_serial_ids,transfer_in_serial_ids,transfer_out_class_num,transfer_in_class_num,transfer_out_class_money,transfer_in_class_money,order_amount,class_num,left_num,transfer_contract_money,transfer_class_num,cancel_time,finish_time,created_at,updated_at,sys_update_dc,transfer_in_time,real_amount,pay_num,pay_time,apply_time,order_id,contract_order_id,schedule,transfer_source', 'transfer_id,contract_id,transfer_contract_id,student_id,parent_id,relationship_id,product_id,product_name,transfer_out_id,transfer_out_name,transfer_in_id,transfer_in_name,transfer_out_teaching_center_id,transfer_out_teaching_center,transfer_in_teaching_center_id,transfer_in_teaching_center,status,reason,transfer_time,approval_time,class_serial_ids,transfer_in_serial_ids,transfer_out_class_num,transfer_in_class_num,transfer_out_class_money,transfer_in_class_money,order_amount,class_num,left_num,transfer_contract_money,transfer_class_num,cancel_time,finish_time,created_at,updated_at,sys_update_dc,transfer_in_time,real_amount,pay_num,pay_time,apply_time,order_id,contract_order_id,schedule,transfer_source', '{\"transfer_id\":\"3993297331471360\",\"contract_id\":\"263631204624133952\",\"transfer_contract_id\":\"0\",\"student_id\":\"14510198323002067294\",\"parent_id\":\"14510198323002067292\",\"relationship_id\":\"-1\",\"product_id\":\"586\",\"product_name\":\"G1-1线上\",\"transfer_out_id\":\"10010657\",\"transfer_out_name\":\"SYZXZBXB1-1线上\",\"transfer_in_id\":\"10010656\",\"transfer_in_name\":\"BJYJ1-1线上\",\"transfer_out_teaching_center_id\":\"239\",\"transfer_out_teaching_center\":\"沈阳在线直播小班\",\"transfer_in_teaching_center_id\":\"237\",\"transfer_in_teaching_center\":\"燕郊在线直播小班\",\"status\":\"-3\",\"reason\":\"与小学上课作息时间冲突\",\"transfer_time\":\"2020-11-17 00:00:00\",\"approval_time\":\"\",\"class_serial_ids\":\"11,12\",\"transfer_in_serial_ids\":\"11,12\",\"transfer_out_class_num\":\"2\",\"transfer_in_class_num\":\"2\",\"transfer_out_class_money\":\"1670\",\"transfer_in_class_money\":\"1670\",\"order_amount\":\"1670\",\"class_num\":\"2\",\"left_num\":\"2\",\"transfer_contract_money\":\"0\",\"transfer_class_num\":\"10\",\"cancel_time\":\"\",\"finish_time\":\"\",\"created_at\":\"2020-11-16 14:16:21\",\"updated_at\":\"2020-11-16 14:16:21\",\"sys_update_dc\":\"2020-11-16 14:16:21\",\"transfer_in_time\":\"2020-12-01 00:00:00\",\"real_amount\":\"1670\",\"pay_num\":\"2\",\"pay_time\":\"\",\"apply_time\":\"2020-11-16 14:16:21\",\"order_id\":\"\",\"contract_order_id\":\"\",\"schedule\":\"[{\\\"id\\\":157604,\\\"class_id\\\":10010656,\\\"start_date\\\":\\\"2020-10-23\\\",\\\"end_date\\\":\\\"2021-08-18\\\",\\\"start_time\\\":\\\"02:05:00\\\",\\\"end_time\\\":\\\"02:50:00\\\",\\\"week_date\\\":2,\\\"is_online\\\":0},{\\\"id\\\":157605,\\\"class_id\\\":10010656,\\\"start_date\\\":\\\"2020-10-23\\\",\\\"end_date\\\":\\\"2021-08-18\\\",\\\"start_time\\\":\\\"03:05:00\\\",\\\"end_time\\\":\\\"03:50:00\\\",\\\"week_date\\\":2,\\\"is_online\\\":0}]\",\"transfer_source\":\"0\"}', NULL, NULL, 0, NULL, '2020-11-16 15:03:05', '2020-11-16 15:03:05');
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '{"name":"文档ID","desc":"哈哈哈哈哈哈","type":"password"}',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '{"name":"上级ID","desc":"","type":"select", "options":{"callback":"getMenuTree"}}',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '{"name":"是否隐藏","options":{"1":"否","2": "是"}}',
  `pathname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路由',
  `iconfont` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '{"name":"图标"}',
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES (200, '菜单列表', 0, 0, 0, '/admin/menu/list.html', 'fa-bars', 1, '2020-02-16 09:14:38', '2020-07-21 21:30:47', 0, '2020-07-21 13:09:05');
INSERT INTO `menu` VALUES (201, '管理员列表', 0, 0, 0, '/admin/admins/list.html', 'fa-user', 1, '2020-02-16 09:14:38', '2020-07-19 21:28:45', 0, NULL);
INSERT INTO `menu` VALUES (203, '资源列表', 0, 0, 0, '/admin/resource/list.html', 'fa-tag', 1, '2020-02-16 09:14:38', '2020-11-15 22:49:37', 0, '2020-11-15 22:48:13');
INSERT INTO `menu` VALUES (205, '权限管理', 0, 0, 0, '', 'fa-share', 1, '2020-02-16 09:14:38', '2020-07-22 13:48:29', 0, NULL);
INSERT INTO `menu` VALUES (206, '权限节点列表', 205, 0, 0, '/admin/permissions/list.html', 'fa-tag', 1, '2020-02-16 09:14:38', '2020-07-22 13:48:33', 0, NULL);
INSERT INTO `menu` VALUES (207, '角色列表', 205, 0, 0, '/admin/roles/list.html', 'fa-tag', 1, '2020-02-16 09:14:38', '2020-07-22 13:48:36', 0, '2020-07-20 22:42:33');
INSERT INTO `menu` VALUES (231, '业务事件数据模型', 0, 0, 0, '', '', 1, '2020-11-12 18:11:19', '2020-11-15 20:38:55', 0, NULL);
INSERT INTO `menu` VALUES (232, '事件列表', 231, 0, 0, '/admin/event/list.html', '', 1, '2020-11-12 18:11:52', '2020-11-12 18:14:26', 0, NULL);
INSERT INTO `menu` VALUES (233, 'binlog数据流', 231, 0, 0, '/admin/event/stream_list.html', '', 1, '2020-11-12 18:26:20', '2020-11-15 20:41:49', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for rbac_permission
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `method` char(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `title` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`permission_id`) USING BTREE,
  KEY `Title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of rbac_permission
-- ----------------------------
BEGIN;
INSERT INTO `rbac_permission` VALUES (1, 'GET', '/mid/rbac/a', 'aaaaaaaaa', 0, NULL);
INSERT INTO `rbac_permission` VALUES (2, 'getList', 'member2', '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (3, 'post', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (4, 'put', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (5, 'delete', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (6, 'list', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (7, 'get', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (8, 'post', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (9, 'put', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (10, 'delete', NULL, '', 1, '2020-07-21 00:24:48');
INSERT INTO `rbac_permission` VALUES (11, 'list', NULL, '', 1, '2020-07-21 13:08:53');
INSERT INTO `rbac_permission` VALUES (12, 'get', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (13, 'post', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (14, 'put', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (15, 'delete', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (16, 'getList', 'member', '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (17, 'get', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (18, 'post', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (19, 'put', NULL, '', 0, NULL);
INSERT INTO `rbac_permission` VALUES (20, 'delete', NULL, '', 1, '2020-07-20 23:57:24');
INSERT INTO `rbac_permission` VALUES (21, 'ret', 'sfsfs/sfsfs', 'aaad', 1, '2020-07-20 23:57:59');
INSERT INTO `rbac_permission` VALUES (22, 'qqq', 'qqq', 'qqq', 0, NULL);
INSERT INTO `rbac_permission` VALUES (23, 'bb', 'cc', 'aa', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  KEY `Title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
BEGIN;
INSERT INTO `rbac_role` VALUES (1, 'root', '超级管理员', 0, NULL);
INSERT INTO `rbac_role` VALUES (2, '基础权限', '没此基础权限的管理员，不能以常规方式登录系统', 0, NULL);
INSERT INTO `rbac_role` VALUES (3, '三级模块管理员', '三级模块管理员', 0, NULL);
INSERT INTO `rbac_role` VALUES (4, '授权人员', '只能授权的管理员', 0, NULL);
INSERT INTO `rbac_role` VALUES (5, '编辑部', '发帖人员', 0, NULL);
INSERT INTO `rbac_role` VALUES (6, '物流部', '查看商品', 0, NULL);
INSERT INTO `rbac_role` VALUES (7, 'qewqeqe', 'ssss', 1, '2020-07-21 00:03:06');
COMMIT;

-- ----------------------------
-- Table structure for rbac_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_permission`;
CREATE TABLE `rbac_role_permission` (
  `role_permissions_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `assignment_date` datetime NOT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_permissions_id`) USING BTREE,
  UNIQUE KEY `role_permission` (`role_id`,`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of rbac_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `rbac_role_permission` VALUES (3, 2, 16, '2020-02-23 12:45:05', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (31, 1, 2, '2020-07-22 05:35:10', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (32, 1, 3, '2020-07-22 05:35:10', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (33, 1, 6, '2020-07-22 05:35:10', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (34, 1, 7, '2020-07-22 05:35:10', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (35, 1, 1, '2020-07-22 06:07:01', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (36, 2, 1, '2020-07-22 06:07:01', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (37, 3, 1, '2020-07-22 06:07:01', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (38, 4, 1, '2020-07-22 06:07:01', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (39, 5, 1, '2020-07-22 06:07:01', 0, NULL);
INSERT INTO `rbac_role_permission` VALUES (40, 6, 1, '2020-07-22 06:07:01', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for rbac_user_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_user_role`;
CREATE TABLE `rbac_user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `assignment_date` datetime NOT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_role_id`) USING BTREE,
  UNIQUE KEY `user_role` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of rbac_user_role
-- ----------------------------
BEGIN;
INSERT INTO `rbac_user_role` VALUES (1, 12345, 1, '2019-05-29 14:32:14', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (2, 12346, 1, '2019-05-29 14:32:14', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (3, 12347, 1, '2019-05-29 14:32:14', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (4, 12345, 14, '2019-05-29 14:32:14', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (19, 3, 1, '2019-05-30 23:35:24', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (20, 2, 4, '2020-02-16 12:52:12', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (21, 2, 3, '2020-02-16 12:52:12', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (22, 2, 1, '2020-02-16 12:52:12', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (30, 1, 1, '0000-00-00 00:00:00', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (31, 1, 2, '0000-00-00 00:00:00', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (32, 1, 4, '0000-00-00 00:00:00', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (33, 1, 5, '0000-00-00 00:00:00', 0, NULL);
INSERT INTO `rbac_user_role` VALUES (34, 1, 6, '0000-00-00 00:00:00', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_group` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分组',
  `resource_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`resource_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resource
-- ----------------------------
BEGIN;
INSERT INTO `resource` VALUES (1, 'admin', '资源', 'Sourcessss', 0, NULL);
INSERT INTO `resource` VALUES (2, 'admin', 'rbac-权限', 'Permissions', 0, NULL);
INSERT INTO `resource` VALUES (3, 'admin', '角色', 'Roles', 0, NULL);
INSERT INTO `resource` VALUES (4, 'admin', '侧边栏', 'Sidebar', 0, NULL);
INSERT INTO `resource` VALUES (5, 'qqss', 'w', 'eess', 1, '2020-07-21 00:16:56');
COMMIT;

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `hah` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `he` int(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`test_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
BEGIN;
INSERT INTO `test` VALUES (10, NULL, 'heihei', NULL, 1, '2020-11-11 16:36:37', '2020-11-11 16:44:30');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
