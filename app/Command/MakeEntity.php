<?php

namespace App\Command;

use App\Foundation\Utils\StringUtil;
use App\Foundation\Utils\TableUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeEntity extends Command
{
    protected $typeMap = [
        'varchar' => 'string',
        'text' => 'string',
        'datetime' => 'string',
        'tinyint' => 'int',
    ];


    protected static $defaultName = 'make:entity';

//    public function __construct()
//    {
//        parent::__construct();
//    }

    protected function configure()
    {
        $this->setDescription('生成entity文件');

        $this->addOption("overWrite", null, InputOption::VALUE_OPTIONAL, "覆盖原entity文件", false);
//        $this->addArgument("overWrite", null, InputOption::VALUE_OPTIONAL, "覆盖原entity文件", false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $overWrite = $input->getOption('overWrite');

            // 获取所有表
            $tables = TableUtil::getTableNames('codeper');

            foreach ($tables as $table) {
                $namespace = "App\Http\Entities";
                $objectName = StringUtil::convertTableToObject($table);
                $entityFile = ROOT . '/app/Http/Entities/' . $objectName . 'Entity.php';

                if (file_exists($entityFile)) {
                    $output->write($entityFile . '<comment>文件已存在</comment>', OutputInterface::OUTPUT_PLAIN);
                    continue;
                }

                $tableField = TableUtil::getTableField('codeper', $table);

                $namespaceObj = new \Nette\PhpGenerator\PhpNamespace($namespace);
                $namespaceObj->addUse('Framework\Foundation\Entity\BaseEntity');

                $class = $namespaceObj->addClass($objectName . 'Entity');
                $class
                    ->setExtends('Framework\Foundation\Entity\BaseEntity');
//                    ->addComment('@Automatic generation');

                foreach ($tableField as $field => $type) {
                    $method = StringUtil::convertTableToObject($field);
                    $method = lcfirst($method);
                    $phpType = $this->getPhpTypeByMysqlType($type);
                    $class->addComment("@method {$phpType} {$method}()");
                }


                $ret = '<?php ' . PHP_EOL . PHP_EOL . $namespaceObj->__toString();
                $putRet = file_put_contents($entityFile, $ret);

                $output->write($entityFile . '<info>生成成功</info>', OutputInterface::VERBOSITY_NORMAL);
            }
        } catch (\Exception $e) {
            echo $e->getMessage(), PHP_EOL;
        }
    }


    protected function getPhpTypeByMysqlType($mysqlType)
    {
        switch ($mysqlType) {
            case "tinyint":
            case "smallint":
            case "int":
            case "integer":
            case "bigint":
            case "numeric":
            case "enum":
                return "int";
                break;
            case "double":
            case "decimal":
                return "float";
                break;
            case "varchar":
            case "char":
            case "text":
                return "float";
                break;
            default:
                return "string";
                break;
        }
    }
}
