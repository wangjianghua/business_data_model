<?php

namespace App\Http\Middleware;

use App\Http\Handler\AdminWebTokenHandler;
use Framework\Foundation\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class AdminJsonWebToken implements MiddlewareInterface
{
    public function getHandler()
    {
        $key = Config::instance()->get('admin.jwt.key');
        return new AdminWebTokenHandler($key);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
