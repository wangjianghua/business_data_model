<?php

namespace App\Http\Middleware;

use App\Http\Handler\RestRbacHandler;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use App\Http\Handler\RbacHandler;

class RestFulRbacMiddleware implements MiddlewareInterface
{
    public function getHandler()
    {
        return new RestRbacHandler();
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
