<?php

namespace App\Http\Middleware;

use Framework\Foundation\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class BrowserCache
{
    public function getHandler()
    {
//        $key = Config::instance()->get('admin.jwt.key');
        return new \App\Http\Handler\BrowserCacheHandler(86400);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
