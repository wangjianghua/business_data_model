<?php

namespace App\Http\Middleware;

use App\Http\Handler\CorsHandler;
use Framework\Foundation\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface
    ;

class CORSMiddleware implements MiddlewareInterface
{
    public function getHandler()
    {
        $config = Config::instance()->get('app.cors');
        return new CorsHandler($config);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
