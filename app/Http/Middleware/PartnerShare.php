<?php

namespace App\Http\Middleware;

use App\Http\Handler\JsonWebTokenHandler;
use App\Http\Handler\PartnerShareHandler;
use Framework\Foundation\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class PartnerShare implements MiddlewareInterface
{
    public function getHandler()
    {
        return new PartnerShareHandler();
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
