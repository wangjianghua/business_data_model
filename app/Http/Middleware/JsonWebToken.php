<?php

namespace App\Http\Middleware;

use App\Http\Handler\JsonWebTokenHandler;
use Framework\Foundation\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class JsonWebToken implements MiddlewareInterface
{
    public function getHandler()
    {
        $secret = Config::instance()->get('app.json_web_token.secret');
        return new JsonWebTokenHandler($secret);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
