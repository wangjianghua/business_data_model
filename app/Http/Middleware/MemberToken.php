<?php

namespace App\Http\Middleware;

use App\Http\Handler\JsonWebTokenHandler;
use Framework\Foundation\Config;
use Framework\Foundation\Response\ResponseGen;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class MemberToken implements MiddlewareInterface
{
    public function getHandler()
    {
        $key = Config::instance()->get('member.jwt.key');
        return new JsonWebTokenHandler($key, false);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
