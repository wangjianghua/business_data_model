<?php

namespace App\Http\Middleware;

use App\Http\Handler\HandlerB;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class TestMiddlewareB implements MiddlewareInterface
{
    public function getHandler()
    {
        return new HandlerB();
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request);
    }
}
