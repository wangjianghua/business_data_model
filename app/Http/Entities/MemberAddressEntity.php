<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int orderAddressId()
 * @method int orderBaseId()
 * @method int memberId()
 * @method float consignee()
 * @method float phone()
 * @method int provinceId()
 * @method int cityId()
 * @method int areaId()
 * @method float detail()
 * @method int isDefault()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class MemberAddressEntity extends BaseEntity
{
}
