<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int roleId()
 * @method float title()
 * @method float description()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class RbacRoleEntity extends BaseEntity
{
}
