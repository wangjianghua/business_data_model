<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int aliNotifyId()
 * @method float outTradeNo()
 * @method float tradeNo()
 * @method float tradeStatus()
 * @method float content()
 * @method string createdAt()
 * @method string updatedAt()
 */
class AliNotifyEntity extends BaseEntity
{
}
