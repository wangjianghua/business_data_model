<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int menuId()
 * @method float title()
 * @method int pid()
 * @method int sort()
 * @method int hide()
 * @method float pathname()
 * @method float iconfont()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class MenuEntity extends BaseEntity
{
}
