<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int administratorId()
 * @method float username()
 * @method float password()
 * @method float name()
 * @method float avatar()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class AdministratorEntity extends BaseEntity
{
}
