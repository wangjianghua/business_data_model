<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int informationId()
 * @method int memberId()
 * @method float province()
 * @method float city()
 * @method float area()
 * @method float typeText()
 * @method float keyword()
 * @method float imageUrl()
 * @method float description()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class InformationEntity extends BaseEntity
{
}
