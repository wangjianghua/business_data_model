<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int aliPayId()
 * @method int outTradeNo()
 * @method float totalAmount()
 * @method float tradeNo()
 * @method float authAppId()
 * @method float appId()
 * @method float sellerId()
 * @method string timestamp()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 */
class AliPayEntity extends BaseEntity
{
}
