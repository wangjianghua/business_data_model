<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int orderBaseId()
 * @method int orderType()
 * @method int memberId()
 * @method float orderNo()
 * @method int totalAmount()
 * @method int goodsAmount()
 * @method int postage()
 * @method float name()
 * @method int status()
 * @method int shareOwner()
 * @method int type()
 * @method float payVoucher()
 * @method int returnRet()
 * @method string returnAt()
 * @method int notifyRet()
 * @method string notifyAt()
 * @method float message()
 * @method int sharedBy()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class OrderBaseEntity extends BaseEntity
{
}
