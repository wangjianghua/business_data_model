<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int clientId()
 * @method int regByMember()
 * @method int regByArticle()
 * @method float username()
 * @method float phone()
 * @method int sex()
 * @method float favor()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class ClientEntity extends BaseEntity
{
}
