<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int testId()
 * @method float name()
 * @method float hah()
 * @method int he()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 */
class TestEntity extends BaseEntity
{
}
