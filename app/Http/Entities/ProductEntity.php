<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int productId()
 * @method float productName()
 * @method float productThumbnail()
 * @method float description()
 * @method float contentDetail()
 * @method int price()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class ProductEntity extends BaseEntity
{
}
