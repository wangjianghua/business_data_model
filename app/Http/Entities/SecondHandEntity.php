<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int secondHandId()
 * @method int memberId()
 * @method int productId()
 * @method float productName()
 * @method float productThumbnail()
 * @method int price()
 * @method float illustration()
 * @method float content()
 * @method float province()
 * @method float city()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class SecondHandEntity extends BaseEntity
{
}
