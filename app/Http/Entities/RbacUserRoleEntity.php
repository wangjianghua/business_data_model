<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int userRoleId()
 * @method int userId()
 * @method int roleId()
 * @method string assignmentDate()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class RbacUserRoleEntity extends BaseEntity
{
}
