<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int aliRefundId()
 * @method float outTradeNo()
 * @method float tradeNo()
 * @method int code()
 * @method float msg()
 * @method float refundFee()
 * @method float sendBackFee()
 * @method float content()
 * @method string createdAt()
 * @method string updatedAt()
 */
class AliRefundEntity extends BaseEntity
{
}
