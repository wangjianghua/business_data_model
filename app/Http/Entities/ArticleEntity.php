<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int articleId()
 * @method int memberId()
 * @method float bannerImage()
 * @method float bannerText()
 * @method float bannerLink()
 * @method float bannerFinal()
 * @method float content()
 * @method float contentLink()
 * @method int status()
 * @method int maxViewTimes()
 * @method int viewTime()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class ArticleEntity extends BaseEntity
{
}
