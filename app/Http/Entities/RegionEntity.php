<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int id()
 * @method float name()
 * @method int parentId()
 * @method float shortName()
 * @method int levelType()
 * @method float cityCode()
 * @method float zipCode()
 * @method float mergerName()
 * @method string lng()
 * @method string lat()
 * @method float pinyin()
 */
class RegionEntity extends BaseEntity
{
}
