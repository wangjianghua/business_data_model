<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int dddEventStreamId()
 * @method float dbName()
 * @method float tableName()
 * @method float transactionTag()
 * @method int eventType()
 * @method float columns()
 * @method float updateColumns()
 * @method float updateValue()
 * @method float ignoreColomnValue()
 * @method float comment()
 * @method int isDeleted()
 * @method string deletedAt()
 * @method string createdAt()
 * @method string updatedAt()
 */
class DddEventStreamEntity extends BaseEntity
{
}
