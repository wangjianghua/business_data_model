<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int memberId()
 * @method float username()
 * @method float password()
 * @method float mobile()
 * @method float email()
 * @method int status()
 * @method float salt()
 * @method string lastLog()
 * @method int sharedBy()
 * @method int vipLevel()
 * @method int accountBalance()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class MemberEntity extends BaseEntity
{
}
