<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int orderGoodId()
 * @method float orderNo()
 * @method int quantity()
 * @method int productId()
 * @method float productName()
 * @method float productThumbnail()
 * @method int price()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class OrderGoodEntity extends BaseEntity
{
}
