<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int id()
 * @method float code()
 * @method float name()
 * @method int isDeleted()
 */
class Kuaidi100Entity extends BaseEntity
{
}
