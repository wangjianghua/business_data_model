<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int resourceId()
 * @method float resourceGroup()
 * @method float resourceName()
 * @method float identity()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class ResourceEntity extends BaseEntity
{
}
