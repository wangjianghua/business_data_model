<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int forumId()
 * @method int memberId()
 * @method float title()
 * @method float content()
 * @method float status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class ForumEntity extends BaseEntity
{
}
