<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int permissionId()
 * @method float method()
 * @method float source()
 * @method float title()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class RbacPermissionEntity extends BaseEntity
{
}
