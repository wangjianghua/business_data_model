<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int orderGoodId()
 * @method int orderBaseId()
 * @method int productId()
 * @method float productName()
 * @method int quantity()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class OrderGoodBcEntity extends BaseEntity
{
}
