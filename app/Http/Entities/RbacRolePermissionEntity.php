<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int rolePermissionsId()
 * @method int roleId()
 * @method int permissionId()
 * @method string assignmentDate()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class RbacRolePermissionEntity extends BaseEntity
{
}
