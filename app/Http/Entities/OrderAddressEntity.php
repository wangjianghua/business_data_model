<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int orderAddressId()
 * @method int orderBaseId()
 * @method float orderNo()
 * @method int memberId()
 * @method float consignee()
 * @method float phone()
 * @method int provinceId()
 * @method float province()
 * @method int cityId()
 * @method float city()
 * @method int areaId()
 * @method float area()
 * @method float detail()
 * @method float expressCompany()
 * @method float expressNumber()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class OrderAddressEntity extends BaseEntity
{
}
