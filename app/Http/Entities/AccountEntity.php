<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int accountId()
 * @method int memberId()
 * @method int accountBalance()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class AccountEntity extends BaseEntity
{
}
