<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int id()
 * @method float bannerImage()
 * @method float bannerText()
 * @method float bannerLink()
 * @method float bannerFinal()
 * @method float content()
 * @method float contentLink()
 * @method int status()
 * @method string createdAt()
 * @method string updatedAt()
 * @method int isDeleted()
 * @method string deletedAt()
 */
class SecretEntity extends BaseEntity
{
}
