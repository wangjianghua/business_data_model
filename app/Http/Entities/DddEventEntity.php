<?php

namespace App\Http\Entities;

use Framework\Foundation\Entity\BaseEntity;

/**
 * @method int dddEventId()
 * @method float eventType()
 * @method float eventTag()
 * @method float eventName()
 * @method float streamIds()
 * @method float eventVersion()
 * @method float comment()
 * @method int isDeleted()
 * @method string deletedAt()
 * @method string createdAt()
 * @method string updatedAt()
 */
class DddEventEntity extends BaseEntity
{
}
