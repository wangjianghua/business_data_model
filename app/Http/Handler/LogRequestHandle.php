<?php

namespace App\Http\Handler;

use Framework\Facade\Log;
use Framework\MiddlewareRunner;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class LogRequestHandle implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = MiddlewareRunner::next($request);

        return $response;
    }
}
