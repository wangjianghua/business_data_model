<?php

namespace App\Http\Handler;

use Framework\MiddlewareRunner;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class HandlerA implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $request = $request->withAttribute("aaa", "bbb");
        $response = MiddlewareRunner::next($request);
        return $response;
    }
}
