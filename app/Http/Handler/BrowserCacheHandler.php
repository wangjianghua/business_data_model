<?php

namespace App\Http\Handler;

use Framework\MiddlewareRunner;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class BrowserCacheHandler implements RequestHandlerInterface
{
    protected $expireTime;

    public function __construct($cacheTime = 86400)
    {
        $this->expireTime = $cacheTime;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = MiddlewareRunner::next($request);
        return $response->withHeader("Cache-Control", 'max-age=' . $this->expireTime)->withHeader('Expires', gmdate("D, d M Y H:i:s", time() + $this->expireTime) . " GMT");
    }
}
