<?php

namespace App\Http\Handler;

use Framework\MiddlewareRunner;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class HandlerB implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = MiddlewareRunner::next($request);
        return $response;
    }
}
