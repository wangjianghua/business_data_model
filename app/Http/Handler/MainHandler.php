<?php

namespace App\Http\Handler;

use App\Exceptions\FrameException;
use App\Exceptions\IllegalRequestException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Framework\Foundation\Response\ResponseGen;
use App\Constants\Common\HttpStatusCode;

class MainHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $dispatchInfo = $request->getAttribute('dispatchInfo');

            list($controller, $method) = explode('@', $dispatchInfo[1]['action']);

            if ('\\' !== substr($controller, 0, 1)) {
                $controllerClass = "\App\Http\Controller\\" . str_replace('/', '\\', ucwords($controller, '/'));
            } else {
                $controllerClass = $controller;
            }

//          if (!class_exists($controllerClass)) {
//              return ResponseGen::error(HttpStatusCode::GONE, "控制器不存在", [$controllerClass]);
//          }
            if (!method_exists($controllerClass, $method)) {
                throw new IllegalRequestException("控制器或目标方法不存在:[$controllerClass, $method]", HttpStatusCode::GONE);
            }

            $response = call_user_func([new $controllerClass, $method], $request);
            if ($response instanceof ResponseInterface) {
                return $response;
            }

            return ResponseGen::success($response);
        } catch (FrameException $e) {
            return ResponseGen::error($e->getCode(), $e->getMessage(), $e->getErrData());
        } catch (\Throwable $throwable) {
            return ResponseGen::error($throwable->getCode(), $throwable->getMessage());
        }
    }
}
