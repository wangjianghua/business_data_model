<?php

namespace App\Http\Handler;

use Framework\MiddlewareRunner;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Cookie\SetCookie;
use Psr\Http\Server\RequestHandlerInterface;

class PartnerShareHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $sharedBy = $request->getIn("shared_by", 0);
        if (!$sharedBy) {
            return MiddlewareRunner::next($request);
        }

        /**
         * @var $response ResponseInterface
         */
        $response = MiddlewareRunner::next($request);

        $cookie = new SetCookie(['Name' => 'shared_by', 'Value' => $sharedBy, 'Expires' => time() + 86400 * 30 * 12]);
        return $response->withHeader('Set-Cookie', $cookie->__toString());
    }
}
