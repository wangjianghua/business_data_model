<?php

namespace App\Http\Rest\Information\Resource;

use App\Exceptions\BusinessException;
use App\Http\Entities\InformationEntity;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Request\ServerRequest;

class Mine
{

    /**
     * @param array $restParams
     * @param $request
     * @return array
     * @throws \Envms\FluentPDO\Exception
     * @throws BusinessException
     */
    public function getList(array $restParams, $request)
    {
        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);

        $where = [];
        $where['is_deleted'] = 0;
        $where['member_id'] = $request->getAttribute('member_id', 0);
   
        $query = FDB::from('information')
            ->where($where)->orderBy('information_id desc');

        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $list = $query->fetchAll();

        if (false === $list) {
            return [];
        }
        foreach ($list as $k => $v) {
            $list[$k]['created_date'] = substr($v['created_at'], 0, 10);
        }

        return ['total' => $total, 'rows' => $list];
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return array
     * @throws \Envms\FluentPDO\Exception
     */
    public function getInfo(array $restParams, $request)
    {
    }


    public function edit()
    {
    }

    public function delete()
    {
    }
}
