<?php


namespace App\Http\Rest\Admin\Resource;

use Framework\Foundation\Database\FDB;
use App\Http\Rest\Admin\DbResource;

class OrderGood extends DbResource
{
    public function getList(array $restParams, $request)
    {
        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);
        $orderNo = $request->getIn('order_no');

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc ')
            ->where('order_no', $orderNo)
            ->where('is_deleted', 0);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ['total' => $total, 'rows' => $ret];
    }
}
