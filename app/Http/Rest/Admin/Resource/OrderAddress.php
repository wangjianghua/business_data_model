<?php


namespace App\Http\Rest\Admin\Resource;

use Framework\Foundation\Database\FDB;
use App\Http\Rest\Admin\DbResource;

class OrderAddress extends DbResource
{
    public function getList(array $restParams, $request)
    {
        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);
        $orderNo = $request->getIn('order_no');

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc ')
            ->where('order_no', $orderNo)
            ->where('is_deleted', 0);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ['total' => $total, 'rows' => $ret];
    }

    public function getInfo(array $restParams, $request)
    {
        if ($restParams['id'] != 0) {
            return parent::getInfo($restParams, $request);
        }

        $orderNo = $request->getIn('order_no');

        $query = FDB::from($restParams['resource'])->where('order_no', $orderNo);
        $info = $query->fetch();
        return $info;
    }

    public function edit(array $restParams, $request)
    {
        // 参数验证
        $params = $request->getParsedBody();
        if (isset($params['_ajax_method'])) {
            unset($params['_ajax_method']);
        }


        $query = FDB::update($restParams['resource'])->where($restParams['field'], $restParams['id']);
        $ret = $query->set($params)->execute();

        return [$ret];
    }
}
