<?php

namespace App\Http\Rest\Admin\Resource;

use Framework\Foundation\Database\FDB;
use Framework\Foundation\Response\ResponseGen;

use App\Http\Rest\Admin\DbResource;

class Product extends DbResource
{
    public function edit(array $restParams, $request)
    {
        // 参数验证
        $params = $request->getParsedBody();
        if (isset($params['_ajax_method'])) {
            unset($params['_ajax_method']);
        }

        if (isset($params['files'])) {
            unset($params['files']);
        }

        $query = FDB::update($restParams['resource'])->where($restParams['field'], $restParams['id']);
        $ret = $query->set($params)->execute();

        if (false === $ret) {
            return ResponseGen::error(400, "列新失败:".$query->getMessage());
        }
        return ResponseGen::success();
    }
}
