<?php

namespace App\Http\Rest\Admin\Resource;

use App\Http\Rest\Admin\MemberDBRest;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class UserRole
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, ServerRequest $request)
    {
        return call_user_func([MemberDBRest::class, 'getList'], $restParams, $request);
    }

    public function getInfo(array $restParams, ServerRequest $request)
    {
        return call_user_func([MemberDBRest::class, 'getInfo'], $restParams, $request);
    }


    public function add(array $restParams, ServerRequest $request)
    {
        return call_user_func([MemberDBRest::class, 'add'], $restParams, $request);
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     */
    public function edit(array $restParams, $request)
    {
        $selectedIds = $request->getIn('selected_ids');
        if (is_null($selectedIds)) {
            return call_user_func([MemberDBRest::class, 'edit'], $restParams, $request);
        }

        FDB::delete($restParams['resource'])->where($restParams['field'], $restParams['id'])->execute();

        if (empty($selectedIds)) {
            return ResponseGen::success();
        }
        $newRoles = [];
        foreach ($selectedIds as $id) {
            $data = [];
            if ($restParams['field'] == 'user_id') {
                $data['user_id'] = $restParams['id'];
                $data['role_id'] = $id;
            } else {
                $data['role_id'] = $restParams['id'];
                $data['user_id'] = $id;
            }
            $newRoles[] = $data;
        }

        FDB::insertInto($restParams['resource'], $newRoles)->execute();
        return ResponseGen::success();
//        echo '<pre>';
//        print_r($restParams);
//        print_r($_POST);
//        print_r($request);
//        print_r($selectedIds);
//        exit('</pre>');
    }


    public function delete(array $restParams, ServerRequest $request)
    {
        return call_user_func([MemberDBRest::class, 'delete'], $restParams, $request);
    }
}
