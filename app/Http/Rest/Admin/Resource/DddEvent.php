<?php


namespace App\Http\Rest\Admin\Resource;

use App\Http\Rest\Admin\DbResource;
use Framework\Foundation\Database\FDB;
use think\facade\Db;
use Framework\Foundation\Response\ResponseGen;

class DddEvent extends DbResource
{
    public function getList(array $restParams, $request)
    {
        $search = $request->getIn('search');

        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);

        $query = Db::table($restParams['resource'])
            ->order($restParams['field'] . ' desc ')
            ->where('is_deleted', '=', 0);
        if (!empty($search)) {
            $query->whereLike('event_name', '%' . $search . '%');
        }

        $total = $query->count();
        $query = $query->limit($pageSize * ($page - 1), $pageSize);
        $ret = $query->select();

        return ResponseGen::success(['total' => $total, 'rows' => $ret]);
    }
}
