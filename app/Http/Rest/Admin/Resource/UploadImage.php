<?php


namespace App\Http\Rest\Admin\Resource;

use App\Exceptions\BusinessException;
use App\Foundation\OssAssistant;
use App\Foundation\PasswordHelper;
use Framework\Facade\ConfigFacade;
use Framework\Foundation\Config;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use GuzzleHttp\Cookie\SetCookie;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

class UploadImage
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, ServerRequest $request)
    {
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, ServerRequest $request)
    {
        /**
         * @var $files \GuzzleHttp\Psr7\UploadedFile[];
         */
        $files = $request->getUploadedFiles();

        $memberId = $request->getAttribute('member_id', 0);
        if (!$memberId) {
            throw new BusinessException("管理员未登录");
        }
        $newNames = [];

        $conf = ConfigFacade::get('app', 'aliyun');
        $oss = new OssAssistant($conf['AccessKey']['id'], $conf['AccessKey']['secret'], $conf['oss_user_upload']['endpoint'], $conf['oss_user_upload']['bucket']);
        foreach ($files as $key => $file) {
            $realname = 'uploads/admin_' . $memberId . '_' . time() . '_' . $files['file']->getClientFilename();
            $path = $oss->putObject($realname, $file->getStream()->getContents());
            $newNames[$key] = $path;
        }

        return ResponseGen::success($newNames);
    }


    public function edit(array $restParams, $request)
    {
    }


    public function delete(array $restParams)
    {
    }
}
