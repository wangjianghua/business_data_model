<?php

namespace App\Http\Rest\Admin\Resource;

use App\Constants\Common\HttpStatusCode;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class Password
{
    public function getList(array $restParams, $request)
    {
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     */
    public function add(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     */
    public function edit(array $restParams, $request)
    {
        $newPassword = $request->getIn('new_password');
        if (empty($newPassword) || strlen($newPassword) < 5) {
            return ResponseGen::error(600, "密码规则不通过");
        }

        $encryptRet = (new \App\Foundation\PasswordHelper())->encryptWord($newPassword);

        FDB::update('administrator')
            ->where('administrator_id', $restParams['id'])
            ->set(['password' => $encryptRet])->execute();
        ;
        return ResponseGen::success();
    }


    public function delete(array $restParams)
    {
    }
}
