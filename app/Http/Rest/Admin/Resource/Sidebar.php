<?php

namespace App\Http\Rest\Admin\Resource;

use Framework\Foundation\Request\ServerRequest;
use App\Foundation\MenuTool;
use Framework\Foundation\Response\ResponseGen;

class Sidebar
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return mixed
     * @see https://www.cnblogs.com/labs/archive/2009/05/12/1455029.html
     * @see http://www.qiling.org/post/224.html
     * @see https://blog.csdn.net/weixin_43972437/article/details/105513486
     */
    public function getList(array $restParams, $request)
    {
        $menu = new MenuTool();
        $menuList = $menu->getSideBar();
        $sideTree = $menu->tree($menuList);

        return $response = ResponseGen::success($sideTree);
//        $response = $response->withHeader("Expires", gmdate("D, d M Y H:i:s", time() + 86400) . " GMT"); // http1.0
//        return $response->withHeader("Cache-Control", 'public,max-age=86400'); //http1.1
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     */
    public function add(array $restParams, $request)
    {
    }


    public function edit(array $restParams, $request)
    {
    }


    public function delete(array $restParams)
    {
    }
}
