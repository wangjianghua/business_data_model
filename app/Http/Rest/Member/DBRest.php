<?php

namespace App\Http\Rest\Member;

use Framework\Foundation\Database\FDB;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Request\ServerRequest;

class DBRest
{
    public function getList(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : -1;

        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc')
            ->where('member_id = ?', $memberId)
            ->where('is_deleted', 0);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ResponseGen::success(['total' => $total, 'rows' => $ret]);
    }

    public function getInfo(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : 0;

        $query = FDB::from($restParams['resource'])
            ->where('member_id = ?', $memberId)
            ->where($restParams['field'], $restParams['id']);
        $info = $query->fetch();
        return ResponseGen::success($info);
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : 0;


        $params = $request->getParsedBody();
        $params['member_id'] = $memberId;
        // 参数验证

        $query = FDB::insertInto($restParams['resource']);
        $ret = $query->values($params)->execute();
        if (!$ret) {
            return ResponseGen::error(400, "添加失败" . $query->getMessage());
        }
        return ResponseGen::success([], "添加成功");
    }


    public function edit(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : 0;

        // 参数验证
        $params = $request->getParsedBody();
        if (isset($params['_ajax_method'])) {
            unset($params['_ajax_method']);
        }

        $query = FDB::update($restParams['resource'])
            ->where('member_id = ?', $memberId)
            ->where($restParams['field'], $restParams['id']);
        $ret = $query->set($params)->execute();

        if (!$ret) {
            return ResponseGen::error(400, "编辑失败:" . $query->getMessage());
        }
        return ResponseGen::success([], "编辑成功");
    }

    public function delete(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : 0;

        $query = FDB::update($restParams['resource'])
            ->where('member_id = ?', $memberId)
            ->where($restParams['field'], $restParams['id']);
        $ret = $query->set([
            'is_deleted' => 1,
            'deleted_at' => date('Y-m-d H:i:s')
        ])->execute();
        if (!$ret) {
            return ResponseGen::error(400, "删除失败");
        }
        return ResponseGen::success([], "删除成功");
    }
}
