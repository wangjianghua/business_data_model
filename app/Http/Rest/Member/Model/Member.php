<?php


namespace App\Http\Rest\Member\Model;

use Framework\Foundation\Database\FDB;

class Member
{
    public function getInfoById($memberId)
    {
        return FDB::from('member')->where('member_id', $memberId)->fetch();
    }

    public function getInfoByMobilePhone($phone)
    {
        return FDB::from('member')->where('mobile', $phone)->fetch();
    }

    public function add($data)
    {
        $query = FDB::insertInto('member')->values($data);
        return $ret = $query->execute();
//        return $query->getQuery();
    }
}
