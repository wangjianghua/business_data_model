<?php

namespace App\Http\Rest\Member;

use App\Constants\Common\HttpStatusCode;
use Framework\Foundation\Config;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Rest\ResourceConverter;
use Framework\Foundation\Rest\RestRequestInfo;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Foundation\Route\RouterHelper;

/**
 * Class AdminRestControl
 * @package App\Http\Rest\Admin
 * @link http://pagination.js.org/  pagenation文档
 */
class MemberRestControl
{
    /**
     * @param $params
     * @param ServerRequest $request
     * @return mixed
     */
    public function index(ServerRequestInterface $request)
    {
        /**
         * @var $restRequestInfo RestRequestInfo;
         */
        $restRequestInfo = $request->getAttribute('restRequestInfo');
        $targetInfo = $this->mapResource($restRequestInfo->getRestTargetInfo());

        $className = "\App\Http\Rest\Member\Resource\\" . $targetInfo['object'];

        if (!$this->checkAllowed($targetInfo['object'])) {
            return ResponseGen::error(HttpStatusCode::FORBIDDEN, "拒绝访问，资源未授权");
        }

        if (class_exists($className)) {
            $obj = new $className;
        } else {
            $obj = new DBRest;
        }

        return call_user_func([$obj, $targetInfo['action']], $targetInfo, $request);
    }

    public function checkAllowed($targetSource)
    {
        $allowed = Config::instance()->get('rest/member.allowed');

        return in_array($targetSource, $allowed['resource']);
    }


    public function mapResource($targetInfo)
    {
        $map = [
            'permission' => 'rbac_permission',
            'role' => 'rbac_role',
            'role_permission' => 'rbac_role_permission',
            'user_role' => 'rbac_user_role',
        ];

        if (isset($map[$targetInfo['resource']])) {
            $targetInfo['resource'] = $map[$targetInfo['resource']];
        }

        return $targetInfo;
    }
}
