<?php


namespace App\Http\Rest\Member\Resource;

use App\Foundation\PasswordHelper;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class Password
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, ServerRequest $request)
    {
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, ServerRequest $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function edit(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $uid = $token->getClaim('uid');

        $password = $request->getIn('password');
        $newPassword = $request->getIn('new_password');

        $userInfo = FDB::from('member')->where('member_id', $uid)->fetch();

        $passwordHelper = new PasswordHelper();
        $checkPassword = $passwordHelper->check($userInfo['password'], $password);
        if (!$checkPassword) {
            return ResponseGen::error(600, "原密码不正确");
        }

        $newWord = $passwordHelper->encryptWord($newPassword);
        FDB::update('member')->where('member_id', $uid)->set(['password' => $newWord])->execute();

        return ResponseGen::success();
    }


    public function delete(array $restParams)
    {
        return ResponseGen::success();
    }
}
