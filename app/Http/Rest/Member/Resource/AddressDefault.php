<?php


namespace App\Http\Rest\Member\Resource;

use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class AddressDefault
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, ServerRequest $request)
    {
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function add(array $restParams, ServerRequest $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function edit(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token->getClaim('uid');

        $defaultId = $request->getIn('default_address_id');

        $ret = FDB::update('member_address')->where('member_id', $memberId)
            ->where('member_address_id', $defaultId)
            ->set(['is_default' => 1])
            ->execute();
        if (!$ret) {
            return ResponseGen::error(600, "设置失败");
        }
        FDB::update('member_address')->where('member_id', $memberId)
            ->where('is_default', 1)
            ->where("member_address_id != ?", $defaultId)
            ->set(['is_default' => 0])
            ->execute();

        return ResponseGen::success([], "设置成功");
    }


    public function delete(array $restParams)
    {
    }
}
