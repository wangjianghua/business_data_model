<?php


namespace App\Http\Rest\Member\Resource;

use Framework\Foundation\Database\FDB;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Request\ServerRequest;

use App\Http\Rest\Member\DBRest;

class OrderBase extends DBRest
{
    public function getList(array $restParams, $request)
    {
        /**
         * @var $token \Lcobucci\JWT\Token;
         */
        $token = $request->getAttribute('jwtToken');
        $memberId = $token ? $token->getClaim('uid') : -1;

        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc')
            ->where('order_type', 1)
            ->where('member_id = ?', $memberId)
            ->where('is_deleted', 0);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ResponseGen::success(['total' => $total, 'rows' => $ret]);
    }
}
