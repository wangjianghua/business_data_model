<?php


namespace App\Http\Rest\Member\Resource;

use App\Foundation\PhoneHelper;
use App\Foundation\RequestFilter;
use App\Foundation\SmsCodeHelper;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class SmsCode
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, ServerRequest $request)
    {
    }

    public function getInfo(array $restParams)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function add(array $restParams, ServerRequest $request)
    {
        $phone = $request->getIn('phone');

        if (!PhoneHelper::checkRule($phone)) {
            throw new \Exception("手机号码无效");
        }

        $requestFilter = new RequestFilter();
        $requestFilter->sendSmsCodeFilter($request, $phone);

        $codeHelper = new SmsCodeHelper();
        $ret = $codeHelper->sendRegisterCode($phone);

        return ResponseGen::success([], "发送成功");
    }


    public function edit(array $restParams, $request)
    {
    }


    public function delete(array $restParams)
    {
    }
}
