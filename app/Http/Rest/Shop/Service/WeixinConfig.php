<?php

namespace App\Http\Rest\Shop\Service;

use Framework\Foundation\Config;

class WeixinConfig
{
    protected $weixinConfig = [
        'use_sandbox' => false, // 是否使用 微信支付仿真测试系统

        'app_id' => '',  // 公众账号ID
//        'sub_appid'    => 'wxxxxxxxx',  // 公众子商户账号ID
        'mch_id' => '', // 商户id
//        'sub_mch_id'   => '123123123', // 子商户id
        'md5_key' => '', // md5 秘钥
        'app_cert_pem' => ROOT . '/important/weixinCert/apiclient_cert.pem',
        'app_key_pem' => ROOT . '/important/weixinCert/apiclient_key.pem',
        'sign_type' => 'MD5', // MD5  HMAC-SHA256
        'limit_pay' => [
            //'no_credit',
        ], // 指定不能使用信用卡支付   不传入，则均可使用
        'fee_type' => 'CNY', // 货币类型  当前仅支持该字段

        'notify_url' => 'https://dayutalk.cn/v1/notify/wx',

        'redirect_url' => 'https://dayutalk.cn/', // 如果是h5支付，可以设置该值，返回到指定页面
    ];

    public function __construct()
    {
        $weixinPayConfig = Config::instance()->get('pay.weixin');
        $this->weixinConfig = array_merge($this->weixinConfig, $weixinPayConfig);
    }

    public function getConfig($key = null)
    {
        if (is_null($key)) {
            return $this->weixinConfig;
        }

        return $this->weixinConfig[$key] ?? null;
    }
}
