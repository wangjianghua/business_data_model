<?php


namespace App\Http\Rest\Shop\Service;

use App\Foundation\Utils\CNYMoney;
use Framework\Foundation\Config;

class AlipayService
{
    protected $aliConfig2 = [
        'use_sandbox' => false, // 是否使用沙盒模式

        'app_id' => '2016073100130857',
        'sign_type' => 'RSA2', // RSA  RSA2


        // 支付宝公钥字符串
        'ali_public_key' => '',

        // 自己生成的密钥字符串
        'rsa_private_key' => '',

        'limit_pay' => [
            //'balance',// 余额
            //'moneyFund',// 余额宝
            //'debitCardExpress',// 	借记卡快捷
            //'creditCard',//信用卡
            //'creditCardExpress',// 信用卡快捷
            //'creditCardCartoon',//信用卡卡通
            //'credit_group',// 信用支付类型（包含信用卡卡通、信用卡快捷、花呗、花呗分期）
        ], // 用户不可用指定渠道支付当有多个渠道时用“,”分隔

        // 与业务相关参数
        'notify_url' => 'https://dayutalk.cn/notify/ali',
        'return_url' => 'https://dayutalk.cn',
    ];

    public function __construct()
    {
        $aliPayConfig = Config::instance()->get('pay.alipay');
        $this->aliConfig2 = array_merge($this->aliConfig2, $aliPayConfig);
        $this->aliConfig2['rsa_private_key'] = $aliPayConfig['private_key'];
    }

    public function getPayUrlById($orderBaseId)
    {
    }

    public function getPayUrlByOrderData($orderNo, $amount, $subject)
    {
        $order = [
            'trade_no' => $orderNo,
            'amount' => CNYMoney::intToHuman($amount), //单位元
            'subject' => $subject,
        ];

        $client = new \Payment\Client(\Payment\Client::ALIPAY, $this->aliConfig2);
        $payUrl = $client->pay(\Payment\Client::ALI_CHANNEL_WEB, $order);
//        header('Location:' . $res);
        return $payUrl;
    }
}
