<?php


namespace App\Http\Rest\Shop\Service;

use Framework\Foundation\Database\FDB;
use Framework\Foundation\Log\Log;

class AliPayReturn implements \Payment\Contracts\IPayNotify
{
    /**
     * 处理自己的业务逻辑，如更新交易状态、保存通知数据等等
     * @param string $channel 通知的渠道，如：支付宝、微信、招商
     * @param string $notifyType 通知的类型，如：支付、退款
     * @param string $notifyWay 通知的方式，如：异步 async，同步 sync
     * @param array $notifyData 通知的数据
     * @return bool
     * @throws \Envms\FluentPDO\Exception
     */
    public function handle(
        string $channel,
        string $notifyType,
        string $notifyWay,
        array $notifyData
    ) {
        Log::info("ali return", ['channel' => $channel, 'notifyTYpe' => $notifyType, 'notifyWay' => $notifyWay, 'data' => $notifyData]);
        $orderNo = $notifyData['out_trade_no'];
        FDB::update('order_base')
            ->where('order_no', $orderNo)
            ->set(['return_ret' => 1, 'return_at' => $notifyData['timestamp']])->execute();

        return true;
    }
}
