<?php


namespace App\Http\Rest\Shop\Service;

use Framework\Foundation\Config;

class AliPayConfig
{
    protected $aliConfig = [
        'use_sandbox' => false, // 是否使用沙盒模式

        'app_id' => '2016073100130857',
        'sign_type' => 'RSA2', // RSA  RSA2


        // 支付宝公钥字符串
        'ali_public_key' => '',

        // 自己生成的密钥字符串
        'rsa_private_key' => '',

        'limit_pay' => [
            //'balance',// 余额
            //'moneyFund',// 余额宝
            //'debitCardExpress',// 	借记卡快捷
            //'creditCard',//信用卡
            //'creditCardExpress',// 信用卡快捷
            //'creditCardCartoon',//信用卡卡通
            //'credit_group',// 信用支付类型（包含信用卡卡通、信用卡快捷、花呗、花呗分期）
        ], // 用户不可用指定渠道支付当有多个渠道时用“,”分隔

        // 与业务相关参数
        'notify_url' => 'https://dayutalk.cn/notify/ali',
        'return_url' => 'https://dayutalk.cn',
    ];

    public function __construct()
    {
        $aliPayConfig = Config::instance()->get('pay.alipay');
        $this->aliConfig = array_merge($this->aliConfig, $aliPayConfig);
        $this->aliConfig['rsa_private_key'] = $aliPayConfig['private_key'];
    }

    public function getConfig($key = null)
    {
        if (is_null($key)) {
            return $this->aliConfig;
        }

        return $this->aliConfig[$key] ?? null;
    }
}
