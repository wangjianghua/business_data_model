<?php


namespace App\Http\Rest\Shop\Resource;

use App\Http\Rest\Shop\BaseResource;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class Region extends BaseResource
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function getList(array $restParams, $request)
    {
        $parentId = $request->getIn('parent_id');

        $list = FDB::from('region')
            ->where('parent_id=?', $parentId)
            ->fetchAll();

        return ResponseGen::success($list);
    }
}
