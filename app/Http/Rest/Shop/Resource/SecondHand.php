<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Http\Rest\Shop\BaseResource;
use Framework\Foundation\Database\FDB;

class SecondHand extends BaseResource
{
    public function add(array $restParams, $request)
    {
        $params = $params = $request->getParsedBody();
        $params['member_id'] = $request->getAttribute('member_id', 0);
        $query = FDB::insertInto($restParams['resource']);
        $ret = $query->values($params)->execute();

        if (!$ret) {
            throw new BusinessException("添加失败:" . $query->getMessage(), 600);
        }
    }
}
