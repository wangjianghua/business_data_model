<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Http\Rest\Shop\BaseResource;
use App\Http\Rest\Shop\Model\ProductModel;
use App\Http\Rest\Shop\Model\RegionModel;
use App\Http\Rest\Shop\Service\AlipayService;
use Framework\Foundation\Request\ServerRequest;

class Recharge
{
    public function getList(array $restParams, $request)
    {
        echo '<pre>';
        print_r('xx');
        exit('</pre>');
        throw new BusinessException("yes", 123, null, ['hha', 'hei']);
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        $orderInfo = $request->getIn('order');

        $orderParams['name'] = "充值";
        $orderParams['order_no'] = time() . rand(1000, 9999);
        $orderParams['total_amount'] = $orderInfo['total_amount'] * 100;
        $orderParams['member_id'] = $request->getAttribute('member_id', 0);
        $orderParams['order_type'] = 2; // 充值

        // 创建订单， 订单和商品关系
        $ret = call_user_func([BaseResource::class, 'resourceAdd'], 'order_base', $orderParams);
        if (!$ret) {
            throw new BusinessException("创建订单失败");
        }

        return $orderParams;
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
        $productId = $request->getIn('product_id');
        if (isset($_SESSION['cart'][$productId])) {
            unset($_SESSION['cart'][$productId]);
        }
    }
}
