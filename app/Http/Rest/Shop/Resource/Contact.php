<?php


namespace App\Http\Rest\Shop\Resource;

use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;

class Contact
{
    public function getList(array $restParams, $request)
    {
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     * @return Response
     */
    public function add(array $restParams, $request)
    {
        $response = ResponseGen::success([], "您的留言又收到，感谢您的反馈");
        return $response;
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
        $productId = $request->getIn('product_id');
        if (isset($_SESSION['cart'][$productId])) {
            unset($_SESSION['cart'][$productId]);
        }
    }
}
