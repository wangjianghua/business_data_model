<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use GuzzleHttp\Cookie\SetCookie;

class ShareTag
{
    public function getList(array $restParams, $request)
    {
        $jump = $request->getIn('jump');
        $sharedBy = $request->getIn('shared_by');
        if (empty($jump)) {
            $jump = '/member/sign_in.html?jump=/shop/share_links.html';
        }

        $response = ResponseGen::locationJump($jump);

        if ($sharedBy) {
            $cookie = new SetCookie(['Name' => 'shared_by', 'Value' => $sharedBy]);
            $response = $response->withHeader('Set-Cookie', $cookie->__toString());
        }
    
        return $response;
    }
}
