<?php

namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Foundation\Utils\CNYMoney;
use App\Http\Rest\Shop\BaseResource;
use App\Http\Rest\Shop\Model\ProductModel;
use App\Http\Rest\Shop\Model\RegionModel;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;

class OrderDetail
{
    public function getList(array $restParams, $request)
    {
    }

    public function getInfo(array $restParams, $request)
    {
        $orderNo = $request->getIn('order_no');

        $orderInfo = FDB::from('order_base')->where('order_no', $orderNo)->fetch();
        if (empty($orderInfo)) {
            throw new BusinessException("请求的资源不存在");
        }

        $orderInfo['human_total_amount'] = CNYMoney::intToHuman($orderInfo['total_amount']);
        $orderInfo['human_goods_amount'] = CNYMoney::intToHuman($orderInfo['goods_amount']);

        $orderGoods = FDB::from('order_good')
            ->where('order_no', $orderNo)
            ->fetchAll();

        return [
            'orderInfo' => $orderInfo,
            'orderGoods' => $orderGoods
        ];
    }

    public function add(array $restParams, $request)
    {
    }

    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
    }
}
