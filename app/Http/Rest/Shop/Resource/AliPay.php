<?php

namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Http\Rest\Shop\BaseResource;
use App\Http\Rest\Shop\Model\ProductModel;
use App\Http\Rest\Shop\Model\RegionModel;
use App\Http\Rest\Shop\Service\AlipayService;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;

class AliPay
{
    public function getList(array $restParams, $request)
    {
        throw new BusinessException("yes", 123, null, ['hha', 'hei']);
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        $orderNo = $request->getIn('order_no');
        $orderParams = FDB::from('order_base')->where('order_no', $orderNo)->fetch();
        if (empty($orderParams)) {
            throw new BusinessException("订单无效");
        }

        $payAmount = $orderParams['total_amount'] * 100;
        // 发起支付
        $payUrl = (new AlipayService())->getPayUrlByOrderData($orderParams['order_no'], $payAmount, $orderParams['name']);

        return $payUrl;
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
        $productId = $request->getIn('product_id');
        if (isset($_SESSION['cart'][$productId])) {
            unset($_SESSION['cart'][$productId]);
        }
    }
}
