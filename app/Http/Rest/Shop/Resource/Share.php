<?php


namespace App\Http\Rest\Shop\Resource;

use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class Share
{
    /**
     * @param array $restParams
     * @param $request ServerRequest;
     */
    public function getList(array $restParams, $request)
    {
        $memberId = $request->getAttribute('member_id', 0);
        if (empty($memberId)) {
            return $response = ResponseGen::locationJump("/member/sign_in.html?jump=/shop/share_links.html");
        }

        $response = ResponseGen::locationJump("/shop/share_links.html");
        return $response;
    }
}
