<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use Framework\Utils\RestUrlBuilder;

class ShareLinks
{
    /**
     * @param array $restParams
     * @param $request ServerRequest;
     */
    public function getList(array $restParams, $request)
    {
        $memberId = $request->getAttribute('member_id', 0);
        if (empty($memberId)) {
            throw new BusinessException("需要先登录");
//            return $response = ResponseGen::locationJump("/member/sign_in.html?jump=/shop/share_links.html");
        }

        $host = $request->getUri()->getScheme() . "://" . $request->getUri()->getHost();
        $shareType = $request->getIn("share_link_type");
        switch ($shareType) {
            case 'shop':
                $link = $host . '/shop/v1/ShareTag?shared_by=' . $memberId . '&jump=/shop/index.html';
                break;
            default:
                $link = $host . '/shop/v1/ShareTag?shared_by=' . $memberId . '&jump=';
                break;
        }

        return ResponseGen::success(['link' => $link]);
    }
}
