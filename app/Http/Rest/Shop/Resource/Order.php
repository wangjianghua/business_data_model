<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Http\Rest\Shop\BaseResource;
use App\Http\Rest\Shop\Model\ProductModel;
use App\Http\Rest\Shop\Model\RegionModel;
use App\Http\Rest\Shop\Service\AlipayService;
use Framework\Foundation\Request\ServerRequest;

class Order
{
    public function getList(array $restParams, $request)
    {
        echo '<pre>';
        print_r($request);
        exit('</pre>');
        throw new BusinessException("yes", 123, null, ['hha', 'hei']);
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        $goods = $request->getIn('goods');
        $address = $request->getIn('address');

        $productIds = array_column($goods, 'product_id');
        $productList = (new ProductModel)->getProductListByIds($productIds);

        $orderGoods = [];

        $orderParams['name'] = "商城下单";
        $orderParams['order_no'] = time() . rand(1000, 9999);
        $orderParams['member_id'] = $request->getAttribute('member_id', 0);
        $orderParams['goods_amount'] = 0;
        foreach ($goods as $goodInfo) {
            if (!isset($productList[$goodInfo['product_id']])) {
                throw new BusinessException("商品不存在" . $goodInfo['product_id']);
            }

            $orderParams['goods_amount'] += $goodInfo['quantity'] * $productList[$goodInfo['product_id']]['price'];

            $orderGoods[$goodInfo['product_id']] = $productList[$goodInfo['product_id']];
            $orderGoods[$goodInfo['product_id']]['order_no'] = $orderParams['order_no'];
            $orderGoods[$goodInfo['product_id']]['quantity'] = $goodInfo['quantity'];
        }

        $orderParams['total_amount'] = $orderParams['goods_amount'];
        $orderParams['message'] = $request->getIn('message', '');

        // 分享人
        $cookies = $request->getCookieParams();
        $orderParams['shared_by'] = $cookies['shared_by'] ?? 0;

        // 创建订单， 订单和商品关系
        $ret = call_user_func([BaseResource::class, 'resourceAdd'], 'order_base', $orderParams);
        if (!$ret) {
            throw new BusinessException("创建订单失败");
        }
        // 添加订单下的商品列表
        $ret2 = call_user_func([BaseResource::class, 'resourceAdd'], 'order_good', $orderGoods);


        $regionModel = new RegionModel();
        // 添加订单收货地址
        $address['order_base_id'] = $ret;
        $address['member_id'] = $request->getAttribute('member_id', 0);
//        $address['province'] = $regionModel->getNameById($address['province_id']);
//        $address['city'] = $regionModel->getNameById($address['city_id']);
//        $address['area'] = $regionModel->getNameById($address['area_id']);
        $addressRet = call_user_func([BaseResource::class, 'resourceAdd'], 'order_address', $address);
        if (!$addressRet) {
            throw new BusinessException("创建收货地址失败");
        }

        return $orderParams;
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
        $productId = $request->getIn('product_id');
        if (isset($_SESSION['cart'][$productId])) {
            unset($_SESSION['cart'][$productId]);
        }
    }
}
