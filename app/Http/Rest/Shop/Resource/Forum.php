<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Http\Rest\Shop\BaseResource;
use App\Http\Rest\Shop\Model\ProductModel;
use App\Http\Rest\Shop\Model\RegionModel;
use App\Http\Rest\Shop\Service\AlipayService;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Entity\EntityBuilder;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;

class Forum
{
    public function getList(array $restParams, $request)
    {
        $memberId = $request->getAttribute('member_id', 0);
        if (empty($memberId)) {
            return [
                'total' => 1,
                'rows' => [
                    [
                        'title' => '秘密交流论坛使用资格',
                        'content' => '会员才能进入交流区。'
                    ]
                ]
            ];
        }

//        $memberEntity = EntityBuilder::genByPkId('member', $memberId, ['vip_level', 'member_id', 'username', 'mobile']);
//        if ($memberEntity->vipLevel() < 1) {
//            return [
//                'total' => 1,
//                'rows' => [
//                    [
//                        'title' => '秘密交流论坛使用资格',
//                        'content' => '在本商城完成订单的会员才能进入交流区。'
//                    ]
//                ]
//            ];
//        }

        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);
        $memberId = $request->getAttribute('member_id', 0);

        if ($memberId == 0) {
            throw new BusinessException("用户未登录");
        }

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc ')
            ->where('is_deleted', 0);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ['total' => $total, 'rows' => $ret];
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        $params = $request->getInArray(['title', 'content']);
        $params['member_id'] = $request->getAttribute('member_id', 0);
        if (empty($params['member_id'])) {
            return ResponseGen::error(600, "需要先登录");
        }

//        $memberEntity = EntityBuilder::genByPkId('member', $params['member_id'], ['vip_level', 'member_id', 'username', 'mobile']);
//        if ($memberEntity->vipLevel() < 1) {
//            return ResponseGen::error(600, "添加帖子权限不足");
//        }
//        $ret = call_user_func([DbResource::class, 'resourceAdd'], 'forum', $params);
//        if (!$ret) {
//            throw new BusinessException("创建订单失败");
//        }

        $db = FDB::insertInto('forum');
        $ret = $db->values($params)->execute();
        return [];
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
    }
}
