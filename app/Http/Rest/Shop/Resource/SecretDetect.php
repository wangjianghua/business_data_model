<?php


namespace App\Http\Rest\Shop\Resource;

use Framework\Foundation\Request\ServerRequest;

class SecretDetect
{
    public function getList(array $restParams, $request)
    {
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
    }


    public function edit(array $restParams, $request)
    {
    }

    public function delete(array $restParams, $request)
    {
    }
}
