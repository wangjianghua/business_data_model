<?php


namespace App\Http\Rest\Shop\Resource;

use App\Http\Entities\OrderBaseEntity;
use App\Http\Rest\Shop\Service\AliPayConfig;
use Framework\Foundation\Log\Log;
use Framework\Foundation\Request\ServerRequest;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Rest\RestRequestInfo;
use Namshi\Cuzzle\Formatter\CurlFormatter;

class AliPayReturn
{
    public function getList(array $restParams, $request)
    {
        return $this->edit($restParams, $request);
    }

    public function getInfo(array $restParams, $request)
    {
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
    }


    /**
     * @param array $restParams
     * @param $request ServerRequest
     */
    public function edit(array $restParams, $request)
    {
        $curlCommand = (new CurlFormatter())->format($request);
        Log::info('return curl :' . $curlCommand);

        /**
         * @var $restRequestInfo RestRequestInfo;
         */
        $restRequestInfo = $request->getAttribute('restRequestInfo');
        $routeInfo = $restRequestInfo->getRouteInfo();
        $route = $routeInfo['route'];
        unset($_GET[$route]);
        $route = trim($route, '/');
        unset($_GET[$route]);

        // 实例化继承了接口的类
        $aliPayConf = new AliPayConfig();
        try {
            $client = new \Payment\Client(\Payment\Client::ALIPAY, $aliPayConf->getConfig());
            $ret = $client->notify(new \App\Http\Rest\Shop\Service\AliPayReturn());
            $orderNo = $request->getIn('out_trade_no');

            $orderInfo = OrderBaseEntity::getByAttr('order_no', $orderNo);

            switch ($orderInfo->orderType()) {
                case 1:
                    return $this->shopOrderReturn($ret, $orderInfo);
                    break;
                case 2:
                    return $this->rechargeReturn($ret, $orderInfo);
                    break;
                case 3:
                    return $this->secondHandReturn($ret, $orderInfo);
                    break;
                default:
                    break;
            }
//            if ($ret == "success") {
//                if (2 == $orderInfo->orderType()) {
//                    return ResponseGen::locationJump("/secret/index.html");
//                }
//
//                if (1 == $orderInfo->orderType()) {
//                    return ResponseGen::locationJump("/shop/pay_success.html?order_no=$orderNo");
//                }
//            } else {
//                if (2 == $orderInfo->orderType()) {
//                    return ResponseGen::locationJump("/secret/index.html");
//                }
//
//                if (1 == $orderInfo->orderType()) {
//                    return ResponseGen::locationJump("/shop/pay_fail.html?order_no=$orderNo");
//                }
//            }

            exit;
        } catch (\Exception $e) {
            Log::error("支付回调发生错误", ['msg' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine()]);
            echo $e->getMessage();
            exit;
        }
    }

    public function shopOrderReturn($ret, $orderInfo)
    {
        if ($ret == "success") {
            return ResponseGen::locationJump("/shop/pay_success.html?order_no=" . $orderInfo['order_no']);
        } else {
            return ResponseGen::locationJump("/shop/pay_fail.html?order_no=" . $orderInfo['order_no']);
        }
    }

    public function rechargeReturn($ret, $orderInfo)
    {
        if ($ret == "success") {
            return ResponseGen::locationJump("/secret/index.html");
        } else {
            return ResponseGen::locationJump("/secret/index.html");
        }
    }

    public function secondHandReturn($ret, $orderInfo)
    {
        if ($ret == "success") {
            return ResponseGen::locationJump("/shop/second/pay_success.html");
        } else {
            return ResponseGen::locationJump("/shop/second/pay_fail.html");
        }
    }


    public function delete(array $restParams, $request)
    {
    }
}
