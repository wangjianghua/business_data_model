<?php


namespace App\Http\Rest\Shop\Resource;

use App\Exceptions\BusinessException;
use App\Foundation\OssAssistant;
use Intervention\Image\ImageManager;

class Dist
{
    public function getList(array $restParams, $request)
    {
        $bannerImage = $request->getIn('banner_image');
        $bannerText = $request->getIn('banner_text');

        $scene = 'dist';

        $maxWidth = 780;
        $manager = new ImageManager();

        $image = $manager->make($this->resolveImage($bannerImage, $scene));


        if ($image->getWidth() > $maxWidth) {
            $image = $image->widen($maxWidth);
        }

        $fontSize = $image->getWidth() / 10;

        if (!empty($bannerText)) {
            $textInfo = $this->getTextPosition($image, $bannerText);
            $image->text($textInfo['text'], $textInfo['x'], $textInfo['y'], function ($font) use ($fontSize) {
                /**
                 * @var $font \Intervention\Image\Gd\Font
                 */
                $font->size($fontSize);
//            $font->color('#fdf6e3');
                $font->align('center');
                $font->file(ROOT . '/public/dist/fonts/SimHei.ttf');
                $font->valign('middle');
            });
        }

        header('Content-type:image/jpeg');
        echo $image->encode();
        exit;
    }

    public function getTextPosition(\Intervention\Image\Image $image, $bannerText, $fontSize = 50)
    {
        $width = $image->getWidth();
        $height = $image->getHeight();

        $strLen = mb_strlen($bannerText);
        $rows = ceil($strLen / 10);
        $text = $this->mb_chunk_split($bannerText, 10, "\n");

        if ($fontSize * $rows > $height) {
            throw new BusinessException("文字太多了");
        }

        $data['text'] = $text;
//        if ($rows > 1) {
//            $data['x'] = $width / 2;
//            $data['y'] = $height / 2;
//        } else {
//            $data['x'] = $width;
//            $data['y'] = $height;
//        }

        $data['x'] = $width / 2;
        $data['y'] = ($height - $fontSize * $rows);

        return $data;
    }

    public function resolveImage($bannerImage, $scene)
    {
        if (strpos($bannerImage, 'lib.exwechat.com')) {
            $object = OssAssistant::extractObjectNameFromUrl($bannerImage);
            return OssAssistant::getObject($object, $scene);
        } else {
            return ROOT . '/public' . $bannerImage;
        }
    }

    /**
     * 分割字符串
     * @param String $string 要分割的字符串
     * @param int $length 指定的长度
     * @param String $end 在分割后的字符串块追加的内容
     * @param bool $once
     * @return string
     */
    public function mb_chunk_split($string, $length, $end, $once = false)
    {
        $array = array();
        $strlen = mb_strlen($string);
        while ($strlen) {
            $array[] = mb_substr($string, 0, $length, "utf-8");
            if ($once) {
                return $array[0] . $end;
            }
            $string = mb_substr($string, $length, $strlen, "utf-8");
            $strlen = mb_strlen($string);
        }
        return implode($end, $array);
    }
}
