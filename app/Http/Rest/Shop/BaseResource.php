<?php

namespace App\Http\Rest\Shop;

use App\Exceptions\BusinessException;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Response\ResponseGen;
use Framework\Foundation\Request\ServerRequest;

class BaseResource
{
    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return array
     * @throws BusinessException
     */
    public function getList(array $restParams, $request)
    {
        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);

        $where = [];
//        $where[$restParams['field']] = $restParams['id'];
        $where['is_deleted'] = 0;

        $pageData = $this->resourceList($restParams['resource'], $page, $pageSize, $where);
        if (empty($pageData)) {
            throw new BusinessException("获取列表数据失败", 600);
        }

        return $pageData;
    }

    public function getInfo(array $restParams, $request)
    {
        return $this->resourceInfo($restParams['resource'], $restParams['field'], $restParams['id']);
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return array
     * @throws BusinessException
     */
    public function add(array $restParams, $request)
    {
        // 参数验证
        $params = $request->getParsedBody();
        $ret = $this->resourceAdd($restParams['resource'], $params);
        
        if (!$ret) {
            throw new BusinessException("添加失败", 600);
        }
        return [];
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return array
     * @throws BusinessException
     */
    public function edit(array $restParams, $request)
    {
        // 参数验证
        $params = $request->getParsedBody();
        if (isset($params['_ajax_method'])) {
            unset($params['_ajax_method']);
        }

        $ret = $this->resourceEdit($restParams['resource'], $restParams['field'], $restParams['id'], $params);
        if (!$ret) {
            throw new BusinessException("编辑失败", 600);
        }
        return [];
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return \Psr\Http\Message\ResponseInterface
     * @throws BusinessException
     */
    public function delete(array $restParams, $request)
    {
        $ret = $this->resourceDelete($restParams['resource'], $restParams['field'], $restParams['id']);
        if (!$ret) {
            throw new BusinessException("删除失败", 600);
        }
        return ResponseGen::success([], "删除成功");
    }


    public function resourceList($table, $page, $pageSize, $where = [])
    {
        $query = FDB::from($table)->where($where);
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $list = $query->fetchAll();

        if (false === $list) {
            return [];
        }

        return ['total' => $total, 'rows' => $list];
    }


    public function resourceInfo($table, $primaryKey, $id)
    {
        return FDB::from($table)->where($primaryKey, $id)->fetch();
    }

    public function resourceAdd($table, $params)
    {
        $query = FDB::insertInto($table);
        $ret = $query->values($params)->execute();
        return $ret;
    }

    public function resourceEdit($table, $primaryKey, $id, $params)
    {
        $query = FDB::update($table)
            ->where($primaryKey, $id);
        return $ret = $query->set($params)->execute();
    }

    public function resourceDelete($table, $primaryKey, $id)
    {
        $query = FDB::update($table)
            ->where($primaryKey, $id);

        return $query->set([
            'is_deleted' => 1,
            'deleted_at' => date('Y-m-d H:i:s')
        ])->execute();
    }
}
