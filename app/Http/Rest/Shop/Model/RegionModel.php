<?php


namespace App\Http\Rest\Shop\Model;

use Framework\Foundation\Database\FDB;

class RegionModel
{
    public function getNameById($id)
    {
        $ret = FDB::from('region')->where('id', $id)->fetch();

        return $ret['name'] ?? '';
    }
}
