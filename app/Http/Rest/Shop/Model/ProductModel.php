<?php


namespace App\Http\Rest\Shop\Model;

use Framework\Foundation\Database\FDB;

class ProductModel
{
    public function getProductListByIds(array $productIds)
    {
        $list = FDB::from('product')
            ->select(['product_id', 'product_name', 'product_thumbnail', 'price'], true)
            ->where('product_id', $productIds)
            ->fetchAll();

        if (empty($list)) {
            return [];
        }

        return array_column($list, null, 'product_id');
    }
}
