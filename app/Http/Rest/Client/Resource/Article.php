<?php


namespace App\Http\Rest\Client\Resource;

use App\Exceptions\BusinessException;
use App\Http\Entities\AccountEntity;
use App\Http\Entities\ArticleEntity;
use App\Http\Entities\MemberEntity;
use App\Http\Entities\SecretEntity;
use App\Http\Rest\Shop\BaseResource;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;

class Article extends BaseResource
{
    public function getList(array $restParams, $request)
    {
        $page = $request->getIn('pageNumber', 1);
        $pageSize = $request->getIn('pageSize', 10);
        $memberId = $request->getAttribute('member_id', 0);

        if ($memberId == 0) {
            throw new BusinessException("用户未登录");
        }

        $query = FDB::from($restParams['resource'])
            ->order($restParams['field'] . ' desc ')
            ->where('is_deleted', 0)
            ->where('member_id', $memberId);

        // 作一些其他参数where条件
        $total = $query->count();
        $query = $query->offset($pageSize * ($page - 1))->limit($pageSize);
        $ret = $query->fetchAll();

        return ['total' => $total, 'rows' => $ret];
    }


    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @return mixed
     * @throws BusinessException
     * @throws \Envms\FluentPDO\Exception
     */
    public function getInfo(array $restParams, $request)
    {
        $info = FDB::from($restParams['resource'])->where($restParams['field'], $restParams['id'])->fetch();

        $memberId = $info['member_id'];
        if (empty($memberId)) {
            throw new BusinessException("001-文章已下架:异常");
        }

        if ($info['is_delete'] == 1) {
            throw new BusinessException("002-文章已删除");
        }

        if ($info['max_view_times'] != 0 && $info['view_time'] >= $info['max_view_times']) {
            throw new BusinessException("003-文章已下架，不可阅读");
        }

        // 是当前用户本人查看文章
        $loginMemberId = $request->getAttribute('member_id');
        if ($loginMemberId == $memberId) {
            return $info;
        }

        // 扣取费用 免费推广使用
//        $memberEntity = MemberEntity::createByAttr('member_id', $memberId);
//        if ($memberEntity->accountBalance() < 1) {
//            throw new BusinessException("001-文章已下架");
//        }
//        MemberEntity::updateByAttr(
//            'member_id',
//            $memberId,
//            ['account_balance' => $memberEntity->accountBalance() - 100]
//        );

        return $info;
    }

    public function preview(array $restParams, $request)
    {
        $memberId = $request->getAttribute('member_id');
        $id = $request->getIn('id');
        $info = FDB::from($restParams['resource'])->where($restParams['field'], $id)->fetch();
        if ($memberId != $info['member_id']) {
            throw new BusinessException("不可预览");
        }

        return $info;
    }

    /**
     * @param array $restParams
     * @param $request ServerRequest
     * @throws \Envms\FluentPDO\Exception
     */
    public function add(array $restParams, $request)
    {
        $params = $request->getParsedBody();
        unset($params['files']);

        $params['member_id'] = $request->getAttribute('member_id');

        $ret = ArticleEntity::insert($params);

        return [$ret];
    }


//    public function edit(array $restParams, $request)
//    {
//    }
//
//    public function delete(array $restParams, $request)
//    {
//    }
}
