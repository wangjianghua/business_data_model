<?php


namespace App\Http\Rest\Client\Resource;

use Framework\Foundation\Response\ResponseGen;
use GuzzleHttp\Cookie\SetCookie;

class Platform
{
    public function getList(array $restParams, $request)
    {
        $jump = $request->getIn('jump');
        $platform = $request->getIn('platform', '无');
        if (empty($jump)) {
            $jump = 'http://shop.exwechat.com/shop/second/index.html';
        }

        $response = ResponseGen::locationJump($jump);

        if ($platform) {
            $cookie = new SetCookie(['Name' => 'platform', 'Value' => $platform, 'Expires' => time() + 86400 * 30]);
            $response = $response->withHeader('Set-Cookie', $cookie->__toString());
        }

        return $response;
    }
}
