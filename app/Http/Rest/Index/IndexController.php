<?php


namespace App\Http\Rest\Index;

use samdark\sitemap\Sitemap;
use samdark\sitemap\Index;

class IndexController
{
    /**
     * @see https://github.com/samdark/sitemap
     * @see https://may90.com/building/changefreq-priority.html
     */
    public function sitemap()
    {
        // create sitemap
        $sitemap = new Sitemap(__DIR__ . '/sitemap.xml');

        // add some URLs
        $sitemap->addItem('http://example.com/mylink1');
        $sitemap->addItem('http://example.com/mylink2', time());
        $sitemap->addItem('http://example.com/mylink3', time(), Sitemap::HOURLY);
        $sitemap->addItem('http://example.com/mylink4', time(), Sitemap::DAILY, 0.3);

        // write it
        $sitemap->write();

        // get URLs of sitemaps written
        $sitemapFileUrls = $sitemap->getSitemapUrls('http://example.com/');

        // create sitemap for static files
        $staticSitemap = new Sitemap(__DIR__ . '/sitemap_static.xml');

        // add some URLs
        $staticSitemap->addItem('http://example.com/about');
        $staticSitemap->addItem('http://example.com/tos');
        $staticSitemap->addItem('http://example.com/jobs');

        // write it
        $staticSitemap->write();

        // get URLs of sitemaps written
        $staticSitemapUrls = $staticSitemap->getSitemapUrls('http://example.com/');

        // create sitemap index file
        $index = new Index(__DIR__ . '/sitemap_index.xml');

        // add URLs
        foreach ($sitemapFileUrls as $sitemapUrl) {
            $index->addSitemap($sitemapUrl);
        }

        // add more URLs
        foreach ($staticSitemapUrls as $sitemapUrl) {
            $index->addSitemap($sitemapUrl);
        }

        // write it
        $index->write();
        
        echo '<pre>';
        print_r('done');
        exit('</pre>');
    }
}
