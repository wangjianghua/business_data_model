<?php

namespace App\Http\Event\Member\Listener;

use Framework\Foundation\Event\Interfaces\EventListenerInterface;
use Framework\Foundation\Event\Listener;

class ListenerB extends Listener implements EventListenerInterface
{
    public function handleEvent(object $event): void
    {
        echo 'event listener b';
    }
}
