<?php

namespace App\Http\Event\Member\Listener;

use Framework\Foundation\Event\Interfaces\EventListenerInterface;
use Framework\Foundation\Event\Listener;

class ListenerA extends Listener implements EventListenerInterface
{
    public function handleEvent(object $event): void
    {
        echo 'in listener A';
    }
}
