<?php


namespace App\Http\Controller\Payment;

use App\Foundation\Utils\CNYMoney;
use App\Http\Rest\Shop\Service\AliPayConfig;
use App\Http\Rest\Shop\Service\WeixinConfig;
use Framework\Foundation\Config;
use Framework\Foundation\Database\FDB;
use Framework\Foundation\Request\ServerRequest;
use Yansongda\Pay\Pay;

class PayController
{
    public function index($request)
    {
        $this->payWeixin($request);
    }

    public function payAli($request)
    {
        $orderId = $request->getIn('order_id');

        $orderInfo = $this->getOrderInfoById($orderId);
        $order = [
            'trade_no' => $orderInfo['order_no'],
            'amount' => CNYMoney::intToHuman($orderInfo['total_amount']), //单位元
            'subject' => $orderInfo['name'],
        ];

        $aliPayConf = new AliPayConfig();
        // 使用
        try {
            $client = new \Payment\Client(\Payment\Client::ALIPAY, $aliPayConf->getConfig());
            $res = $client->pay(\Payment\Client::ALI_CHANNEL_WEB, $order);
            header('Location:' . $res);
            exit;
        } catch (\InvalidArgumentException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Payment\Exceptions\GatewayException $e) {
            echo $e->getMessage();
            var_dump($e->getRaw());
            exit;
        } catch (\Payment\Exceptions\ClassNotFoundException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * @param $request ServerRequest
     * WX_CHANNEL_PUB 也就是JSAPI jssdk支付，必须有openid
     */
    public function payWeixin($request)
    {
        $orderId = $request->getIn('order_id');

        $orderInfo = $this->getOrderInfoById($orderId);
        $serverParams = $request->getServerParams();
        $order = [
            'subject' => $orderInfo['name'],
            'body' => '123',
            'trade_no' => $orderInfo['order_no'],
            'amount' => CNYMoney::intToHuman($orderInfo['total_amount']), //单位元
            'client_ip' => $serverParams['REMOTE_ADDR'],
            'time_expire' => time() + 3600,
        ];

        $weixinConf = new WeixinConfig();
//        echo '<pre>';
//        print_r( $weixinConf );
//        exit('</pre>');
        // 使用
        try {
            $client = new \Payment\Client(\Payment\Client::WECHAT, $weixinConf->getConfig());
            $res = $client->pay(\Payment\Client::WX_CHANNEL_QR, $order);
            echo '<pre>';
            print_r($res);
            exit('</pre>');
            header('Location:' . $res);
            exit;
        } catch (\InvalidArgumentException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Payment\Exceptions\GatewayException $e) {
            echo $e->getMessage();
            var_dump($e->getRaw());
            exit;
        } catch (\Payment\Exceptions\ClassNotFoundException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function index22($request)
    {
        $orderId = $request->getIn('order_id');

        $orderInfo = $this->getOrderInfoById($orderId);
        $order = [
            'out_trade_no' => $orderInfo['order_no'],
//            'total_amount' => CNYMoney::humanToInt($orderInfo['total_amount']), //单位元
            'total_amount' => CNYMoney::intToHuman($orderInfo['total_amount']), //单位元
            'subject' => $orderInfo['name'],
        ];

        $this->payAlipay($order);
        exit;
    }

    public function getOrderInfoById($orderId)
    {
        $ret = FDB::from('order_base')
            ->where('order_base_id=?', $orderId)
            ->fetch();

        return $ret;
    }
}
