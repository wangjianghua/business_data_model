<?php


namespace App\Http\Controller;

use Intervention\Image\ImageManager;

class ImageController
{
    public function display()
    {
        $manager = new ImageManager();
        $image = $manager->make(ROOT . "/public/dist/images/123.jpeg")
            ->resize(500, 300)
            ->text("中文一二三\n中文一二三\n中文一二三\n中文一二三\n中文一二三", 250, 150, function ($font) {
                /**
                 * @var $font \Intervention\Image\Gd\Font
                 */
                $font->size(50);
//            $font->color('#fdf6e3');
                $font->align('center');
                $font->file(ROOT . '/public/dist/fonts/SimHei.ttf');
                $font->valign('middle');
            });
        header('Content-type:image/jpeg');
        echo $image->encode();
        exit;
    }


    public function save()
    {
        $manager = new ImageManager();
        $image = $manager->make(ROOT . "/public/dist/images/123.jpeg")
            ->resize(500, 300)
            ->text("中文一二三\n中文一二三\n中文一二三\n中文一二三\n中文一二三", 250, 150, function ($font) {
                /**
                 * @var $font \Intervention\Image\Gd\Font
                 */
                $font->size(50);
//            $font->color('#fdf6e3');
                $font->align('center');
                $font->file(ROOT . '/public/dist/fonts/SimHei.ttf');
                $font->valign('middle');
            });

        $image->save();
    }
}
