<?php

namespace App\Exceptions;

// 用户层使用
// 多用于控制器，Service， DB层
// 业务逻辑错误，从库里拿到的数据不支持业务逻辑正常执行下去！！


use Throwable;

class BusinessException extends FrameException
{
    public function __construct(string $message = "", int $code = 600, Throwable $previous = null, $errData = null)
    {
        parent::__construct($message, $code, $previous, $errData);
    }
}
