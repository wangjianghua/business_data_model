<?php

namespace App\Exceptions;

// 用户层使用
// 用户的入参有错误
// 在控制器层，Service层输出
class IllegalArgumentException extends FrameException
{
}
