<?php

namespace App\Exceptions;

// 系统层，用户层使用
// 致命错误， 不可正常运行，没有清晰的定义，可用此异常类
class FatalException extends FrameException
{
}
