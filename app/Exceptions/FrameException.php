<?php

namespace App\Exceptions;

use Throwable;

class FrameException extends \Exception
{
    protected $errData;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, $errData = null)
    {
        parent::__construct($message, $code, $previous);

        $this->errData = $errData;
    }

    public function getErrData()
    {
        return $this->errData;
    }
}
