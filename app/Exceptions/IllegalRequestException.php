<?php

namespace App\Exceptions;

// 系统层，用户层使用
// 请求路由有错误
// 在系统层和前置中间件层输出
class IllegalRequestException extends FrameException
{
}
