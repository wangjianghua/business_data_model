<?php

namespace App\Exceptions;

// 用户层使用
// 从第三方返回的数据错误，在调用CURL后未获得预期数据输出
use Throwable;

class ApiDataInvalidException extends FrameException
{
}
