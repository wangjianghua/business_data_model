<?php

namespace App\Exceptions;

use Psr\Http\Message\ResponseInterface;

// 系统层，用户层使用
// 业务流程不可正常运行
class StopWithResponseException extends FrameException
{
    /**
     * @var $response ResponseInterface
     */
    protected $response;

    public function __construct(int $code = 600, string $message = '', ResponseInterface $response = null)
    {
        $this->response = $response;
        parent::__construct($message, $code, $this);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
