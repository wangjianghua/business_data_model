<?php

namespace App\Foundation;

use App\Exceptions\BusinessException;
use Framework\Facade\ConfigFacade;
use Framework\Foundation\Log\Log;
use OSS\Core\OssException;
use OSS\OssClient;

class OssAssistant
{
    protected $accessKeyId;
    protected $accessKeySecret;
    protected $endpoint;
    protected $bucket;

    public function __construct($accessKeyId, $accessKeySecret, $endpoint, $bucket = null)
    {
        $this->accessKeyId = $accessKeyId;
        $this->accessKeySecret = $accessKeySecret;
        $this->endpoint = $endpoint;
        $this->bucket = $bucket;
    }

    public function updateFile($objectName, $filePath, $buck = null)
    {
        if (empty($objectName)) {
            throw new BusinessException("OSS上传文件名必须");
        }
        if (empty($filePath)) {
            throw new BusinessException("OSS上传文件地址必须");
        }
//
//        $conf = ConfigFacade::get('app', 'aliyun');
//
//        $accessKeyId = $conf['AccessKey']['id'];
//        $accessKeySecret = $conf['AccessKey']['secret'];
//        $endpoint = $conf['oss']['endpoint'];
        $bucket = $buck ?? $this->bucket;

        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
            $ret = $ossClient->uploadFile($bucket, $objectName, $filePath);

            return $ret['oss-request-url'] ?? false;
        } catch (OssException $e) {
            Log::error("上传资源失败");
            return false;
        }
    }

    public function putObject($objectName, $content, $buck = null)
    {
        if (empty($objectName)) {
            throw new BusinessException("OSS上传文件名必须");
        }
        if (empty($content)) {
            throw new BusinessException("OSS上传文件地址必须");
        }

        $bucket = $buck ?? $this->bucket;

        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
            $ret = $ossClient->putObject($bucket, $objectName, $content);

            return $ret['oss-request-url'] ?? false;
        } catch (OssException $e) {
            Log::error("上传资源失败");
            return false;
        }
    }


    public static function getObject($objectName, $scene = 'userUpload')
    {
        if (empty($objectName)) {
            throw new BusinessException("OSS文件名必须");
        }

        $conf = ConfigFacade::get('app', 'aliyun');

        $accessKeyId = $conf['AccessKey']['id'];
        $accessKeySecret = $conf['AccessKey']['secret'];

        if ($scene == "userUpload") {
            $endpoint = $conf['oss_user_upload']['endpoint'];
            $bucket = $buck ?? $conf['oss_user_upload']['bucket'];
        } else {
            $endpoint = $conf['oss_domain_dist']['endpoint'];
            $bucket = $buck ?? $conf['oss_domain_dist']['bucket'];
        }

        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ret = $ossClient->getObject($bucket, $objectName);
            return $ret;
        } catch (OssException $e) {
            echo '<pre>';
            print_r($e->getMessage());
            print_r($e->getCode());
            exit('</pre>');
            return false;
        }
    }

    public static function extractObjectNameFromUrl($url, $host = 'http://lib.exwechat.com/')
    {
        return str_replace($host, '', $url);
    }
}
