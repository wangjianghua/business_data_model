<?php

namespace App\Foundation;

use Framework\Foundation\Database\FDB;

class MenuTool
{
    public function getSideBar()
    {
        return FDB::from('menu')->where('status > ?', 0)
            ->where('is_deleted', 0)
            ->fetchAll();
    }

    /**
     * @param $arr
     * @param int $pid
     * @param int $level
     * @return array
     * @see https://www.jb51.net/article/157124.htm
     */
    public function tree($arr, $pid = 0, $level = 0)
    {
        if ($level == 10) {
            return [];
        }
        $res = [];
        foreach ($arr as $v) {
            if ($v['pid'] == $pid) {
                //说明找到，先保存，然后在递归查找
                $v['level'] = $level;

                $child = $this->tree($arr, $v['menu_id'], $level + 1);
                if (!empty($child)) {
                    $v['_child'] = $child;
                }
                $res[] = $v;
            }
        }
        return $res;
    }
}
