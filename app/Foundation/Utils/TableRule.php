<?php

namespace App\Common\Utils;

class TableRule
{
    public $dir;
    public function __construct()
    {
        $this->dir = ROOT . '/app/TableRules/Default';
    }

    public function getTableHead($tableName, $filedShow=[])
    {
        $ret = $this->getRuleFileName($tableName);
        if (empty($ret)) {
            return $filedShow;
        }
        return $ret['tableHead'];
//        $filedRet = array_merge($ret['tableHead'], $filedShow);



        return $filedRet;
    }

    public function getRuleFileName($tableName)
    {
        static $rules = [];
        if (isset($rules[$tableName])) {
            return $rules[$tableName];
        }
        $file = $this->dir . '/' . $tableName . '.php';
        if (file_exists($file)) {
            $rules[$tableName] = include  $file;
            return $rules[$tableName];
        }
        return [];
    }
}
