<?php

namespace App\Foundation\Utils;

use Money\Money;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Currencies\ISOCurrencies;
use Money\Parser\DecimalMoneyParser;
use Money\Currency;

class CNYMoney
{
    public static function intToHuman(int $amount)
    {
        $money = Money::CNY($amount);
        $moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());
        return $moneyFormatter->format($money);
    }

    public static function humanToInt(string $amount)
    {
        $parser = new DecimalMoneyParser(new ISOCurrencies());
        $money = $parser->parse($amount, new Currency('CNY'));
        return $money->getAmount();
    }
}
