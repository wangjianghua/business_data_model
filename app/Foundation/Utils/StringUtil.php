<?php

namespace App\Foundation\Utils;

class StringUtil
{
    public static function convertTableToObject($resource)
    {
        return str_replace('_', '', ucwords($resource, '_'));
    }

    public static function convertObjectToTable($objectName)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1_$2", $objectName));
    }

    public static function getNameBySpaceName($name)
    {
        $pos = strrpos($name, '\\');
        if (false == $pos) {
            return $name;
        }

        return substr($name, $pos + 1);
    }
}
