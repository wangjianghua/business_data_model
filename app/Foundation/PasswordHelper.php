<?php


namespace App\Foundation;

class PasswordHelper
{
    protected $hashAlgo = "sha1";

    public function __construct($hashAlgo = "sha1")
    {
        $this->hashAlgo = $hashAlgo;
    }

    public function encryptWord($word, $salt = null)
    {
        if (!is_null($salt)) {
            $word .= $salt;
        }

        return hash($this->hashAlgo, $word);
    }

    public function check($hashStr, $word, $salt = null)
    {
        return $hashStr === $this->encryptWord($word, $salt);
    }

    public function checkRule($password)
    {
        if (strlen($password) < 4) {
            return false;
        }

        return true;
    }
}
