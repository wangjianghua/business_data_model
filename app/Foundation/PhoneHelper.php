<?php


namespace App\Foundation;

class PhoneHelper
{
    public static function checkRule($phone)
    {
        return preg_match("/^1[0-9]\d{9}$/", $phone);
    }
}
