<?php

namespace App\Foundation\Rbac;

use Framework\Foundation\Database\FDB;

class Permission
{
    public function getPermissionsByUserId($userId)
    {
        $roles = (new Role())->getRolesByUserId($userId);

        return $permissionIds = $this->getPermissionIdsByRoleIds($roles);
    }

    public function getPermissionIdsByRoleIds(array $roleIds)
    {
        $ret = FDB::from('rbac_role_permissions')
            ->select('distinct permission_id', true)
            ->where('role_id', $roleIds)
            ->fetchAll();

        if (empty($ret)) {
            return [];
        }

        return array_column($ret, 'permission_id');
    }

    public function getPermissionsByIds(array $permissionIds)
    {
        $ret = FDB::from('rbac_permissions')
            ->where('permission_id', $permissionIds)
            ->fetchAll();

        return $ret;
    }
}
