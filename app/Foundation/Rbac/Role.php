<?php

namespace App\Foundation\Rbac;

use Framework\Foundation\Database\FDB;

class Role
{
    public function getRolesByUserId($userId)
    {
        $ret = FDB::from('rbac_user_roles')->where('user_id', $userId)
            ->fetchAll();
        if (!$ret) {
            return false;
        }

        return array_column($ret, 'role_id');
    }
}
