<?php


namespace App\Constants;

class Sex extends \Framework\Foundation\Enum
{
    protected $enumsData = [
        'keys' => [
            "man" => 1,
            "woman" => 2,
            "unknown" => 3,
        ],
        'desc' => [
            1 => "男",
            2 => "女",
            3 => "未知"
        ]
    ];
}
