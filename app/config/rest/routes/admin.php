<?php

/**
 * @see https://segmentfault.com/a/1190000014508613
 */
//return [
//    'groups' => [
//        [
//            'prefix' => '/adminRest/v1',
//            'middleware' => [
//            ],
//            'routes' => [
//                [
//                    ['GET', 'POST', 'DELETE', 'PUT'],
//                    '/token',
//                    [
//                        'action' => 'Admin\AdminRestControl@index',
//                    ],
//
//                ],
//            ]
//        ],
//        [
//            'prefix' => '/adminRest/v1',
//            'middleware' => [
//                \App\Http\Middleware\AdminJsonWebToken::class,
//                \App\Http\Middleware\BrowserCache::class
//            ],
//            'routes' => [
//                [
//                    ['GET', 'POST', 'DELETE', 'PUT'],
//                    '/Sidebar',
//                    [
//                        'action' => 'Admin\AdminRestControl@index',
//                    ],
//
//                ],
//            ]
//        ],
//
//        [
//            'prefix' => '/adminRest/v1',
//            'middleware' => [
//                \App\Http\Middleware\AdminJsonWebToken::class
//            ],
//            'routes' => [
//                [
//                    ['GET', 'POST', 'DELETE', 'PUT'],
//                    '/{resource}[/{id}[/{relation}]]',
//                    [
//                        'action' => 'Admin\AdminRestControl@index',
//                    ],
//
//                ],
//            ]
//        ]
//    ],
//];
