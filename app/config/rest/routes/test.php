<?php

/**
 * @see https://segmentfault.com/a/1190000014508613
 */
//return [
//    'routes' => [
//        // 这三个一组，等于下面一个，可以更灵活的方式定义
////        [['GET', 'POST', 'DEL', 'PUT'], '/admin/v1/{resource}/{id}/{relation}', 'RestControl'],
////        [['GET', 'POST', 'DEL', 'PUT'], '/admin/v1/{resource}/{id}', 'RestControl'],
////        [['GET', 'POST', 'DEL', 'PUT'], '/admin/v1/{resource}', 'RestControl'],
//        [
//            ['GET', 'POST', 'DEL', 'PUT'],
//            '/demo/v1/{resource}[/{id}[/{relation}]]',
//            [
//                'action' => '\App\Http\Rest\Demo\RestControl@index',
//                'middleware' => [
//                ]
//            ]
//        ],
////        [
////            ['GET', 'POST', 'DEL', 'PUT'],
////            '/adminRest/v1/{resource}[/{id}[/{relation}]]',
////            [
////                'action' => 'Admin\AdminRestControl@index',
////                'middleware' => [
////                ]
////            ]
////        ],
//        [
//            ['GET', 'POST', 'DEL', 'PUT'],
//            '/rbac/v1/{resource}[/{id}[/{relation}]]',
//            [
//                'action' => 'Demo\RestControl',
//                'middleware' => [
//                    \App\Http\Middleware\JsonWebToken::class,
//                    \App\Http\Middleware\RestFulRbacMiddleware::class,
//                ]
//            ]
//        ],
//
//        ['POST', '/dddd/df', ['action' => 'ff@aa', 'middleware' => []]]
//    ]
//];
