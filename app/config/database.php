<?php

return [
    'mysql' => [
        'default' => [
            'dsn' => \Framework\Foundation\Ini::get('mysql.default.dsn'),
            'username' => \Framework\Foundation\Ini::get('mysql.default.username'),
            'password' => \Framework\Foundation\Ini::get('mysql.default.password')
        ],
    ],


    'redis' => [
        'default' => [
            'host' => \Framework\Foundation\Ini::get('redis.default.host'),
            'port' => \Framework\Foundation\Ini::get('redis.default.port'),
            'password' => \Framework\Foundation\Ini::get('redis.default.password'),
        ]
    ],

    'tp_config' => [
        'default' => 'primary',
        'connections' => [
            'primary' => [
                // 数据库类型
                'type' => 'mysql',
                // 服务器地址
                'hostname' => \Framework\Foundation\Ini::get('mysql.primary.hostname'),
                // 数据库名
                'database' => \Framework\Foundation\Ini::get('mysql.primary.database'),
                // 数据库用户名
                'username' => \Framework\Foundation\Ini::get('mysql.primary.username'),
                // 数据库密码
                'password' => \Framework\Foundation\Ini::get('mysql.primary.password'),
                // 数据库连接端口
                'hostport' => \Framework\Foundation\Ini::get('mysql.primary.hostport', 3306),
                // 数据库连接参数
                'params' => [],
                // 数据库编码默认采用utf8
                'charset' => \Framework\Foundation\Ini::get('mysql.primary.charset', 'utf8'),
                // 数据库表前缀
                'prefix' => \Framework\Foundation\Ini::get('mysql.primary.prefix', ''),
            ]
        ]
    ]
];
