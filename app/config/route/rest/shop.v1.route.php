<?php

use Framework\Foundation\Route\Router;

// 第一组 公开访问
Router::groupStart('/shop/v1', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberToken::class
    ]
]);
Router::get('/AliPayReturn', 'Shop\ShopRestControl@index');
Router::post('/AliPayNotify', 'Shop\ShopRestControl@index');
Router::get('/product[/{id}]', 'Shop\ShopRestControl@index');
Router::get('/second_hand[/{id}]', 'Shop\ShopRestControl@index');

Router::groupStop();

// 第二组， 登录成功后访问
Router::groupStart('/shop/v1', [
    'middleware' => [
        \App\Http\Middleware\PartnerShare::class,
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberAuth::class
    ]
]);

Router::rest('/Share[/{id}]', 'Shop\ShopRestControl@index');
Router::rest('/ShareLinks[/{id}]', 'Shop\ShopRestControl@index');
Router::rest('/Forum[/{id}]', 'Shop\ShopRestControl@index');
Router::rest('/Article[/{id}]', 'Shop\ShopRestControl@index');
Router::rest('/Client[/{id}]', 'Shop\ShopRestControl@index');
Router::rest('/Recharge', 'Shop\ShopRestControl@index');
Router::addRoute(['POST', 'PUT', 'DELETE'], '/SecondHand[/{id}]', 'Shop\ShopRestControl@index');
Router::groupStop();


// 最后的登录成功后的 通用匹配
// 最后的路由正则匹配路由，要求最严格的权限控制
Router::any('/shop/v1/{resource}[/{id}[/{relation}]]', 'Shop\ShopRestControl@index', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberAuth::class
    ]
]);
