<?php

use Framework\Foundation\Route\Router;

// 第一组 公开访问
Router::groupStart('/information/v1', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberToken::class

    ]
]);

Router::addRoute(['OPTIONS', 'GET'], '/information[/{id}]', 'Information\InformationRestControl@index');
Router::rest('/UploadImage[/{id}]', 'Information\InformationRestControl@index');
Router::groupStop();


// 第二组， 登录成功后访问
Router::groupStart('/information/v1', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberAuth::class
    ]
]);

Router::addRoute(['POST', 'DELETE', 'PUT'], '/information[/{id}]', 'Information\InformationRestControl@index');
Router::rest('/mine', 'Information\InformationRestControl@index');
Router::groupStop();


// 最后的登录成功后的 通用匹配
// 最后的路由正则匹配路由，要求最严格的权限控制
Router::any('/information/v1/{resource}[/{id}[/{relation}]]', 'Information\InformationRestControl@index', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class
    ]
]);
