<?php

use Framework\Foundation\Route\Router;

// 登录注册等路由, 公开访问
Router::groupStart('/member/v1', ['middleware' => [\App\Http\Middleware\CORSMiddleware::class]]);
Router::rest('/token', 'Member\MemberRestControl@index');
Router::addRoute(['POST', 'OPTIONS'], '/member', 'Member\MemberRestControl@index');
Router::post('/SmsCode', 'Member\MemberRestControl@index');
Router::groupStop();


// 登录成功，最后的匹配, 要求最严格的权限控制
Router::any(
    '/member/v1/{resource}[/{id}[/{relation}]]',
    'Member\MemberRestControl@index',
    [
        'middleware' => [
            \App\Http\Middleware\CORSMiddleware::class,
            \App\Http\Middleware\MemberAuth::class
        ]
    ]
);
