<?php

use Framework\Foundation\Route\Router;

Router::groupStart('/client/v1', ['middleware' => [\App\Http\Middleware\CORSMiddleware::class]]);
Router::rest('/ShareLinks[/{id}]', 'Client\ClientRestControl@index');
Router::get('/platform', 'Client\ClientRestControl@index');
Router::groupStop();


Router::groupStart('/client/v1', ['middleware' => [\App\Http\Middleware\MemberAuth::class]]);
Router::addRoute(['POST', 'DELETE', 'PUT'], '/Article[/{id}]', 'Client\ClientRestControl@index');
Router::groupStop();

Router::groupStart('/client/v1', ['middleware' => [\App\Http\Middleware\MemberToken::class]]);
Router::get('/Article[/{id}]', 'Client\ClientRestControl@index');
Router::groupStop();


// 最后的登录成功后的 通用匹配
// 最后的路由正则匹配路由，要求最严格的权限控制
Router::any('/client/v1/{resource}[/{id}[/{relation}]]', 'Client\ClientRestControl@index', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\AdminJsonWebToken::class
    ]
]);
