<?php

use Framework\Foundation\Route\Router;

// 注册登录路无，无权限验证
//Router::group(
//    '/adminRest/v1',
//    function () {
//        Router::rest('/token', 'Admin\AdminRestControl@index');
//    }
//);

// 登录完成后的一些需要特殊处理的路由
//Router::group(
//    '/adminRest/v1',
//    function () {
//        // 设置浏览器缓存的路由
//        Router::any('/Sidebar', 'Admin\AdminRestControl@index',
//            ['middleware' => [\App\Http\Middleware\BrowserCache::class]]);
//    },
//    [
//        'middleware' => [
//            \App\Http\Middleware\AdminJsonWebToken::class
//        ]
//    ]
//);

// 注册登录路无，无权限验证
Router::groupStart('/adminRest/v1');
Router::rest('/token', 'Admin\AdminRestControl@index');
Router::groupStop();

// 登录完成后的一些需要特殊处理的路由
Router::groupStart('/adminRest/v1', ['middleware' => [\App\Http\Middleware\AdminJsonWebToken::class]]);
Router::any('/Sidebar', 'Admin\AdminRestControl@index', ['middleware' => [\App\Http\Middleware\BrowserCache::class]]);
// 最后的路由正则匹配路由，要求最严格的权限控制 也可以写到这里
Router::groupStop();


// 最后的登录成功后的 通用匹配
// 最后的路由正则匹配路由，要求最严格的权限控制
Router::any('/adminRest/v1/{resource}[/{id}[/{relation}]]', 'Admin\AdminRestControl@index', [
    'middleware' => [
        \App\Http\Middleware\AdminJsonWebToken::class
    ]
]);
