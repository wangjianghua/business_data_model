<?php

use Framework\Foundation\Route\Router;

// 第一组 公开访问
Router::groupStart('/index/v1');
Router::rest('/weixin', 'Index\WeixinController@index');
Router::rest('/callback', 'Index\WeixinController@callback');
Router::rest('/sitemap', 'Index\IndexController@sitemap');
Router::groupStop();


// 最后的登录成功后的 通用匹配
// 最后的路由正则匹配路由，要求最严格的权限控制
Router::any('/index/v1/{resource}[/{id}[/{relation}]]', 'Shop\ShopRestControl@index', [
    'middleware' => [
        \App\Http\Middleware\CORSMiddleware::class,
        \App\Http\Middleware\MemberToken::class
    ]
]);
