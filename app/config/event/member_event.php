<?php

return [

    [
        'event' => \App\Http\Event\Member\MemberRegister::class,
        'listeners' => [
            [
                'name' => \App\Http\Event\Member\Listener\ListenerA::class,
                'async' => false,
            ]
        ]
    ],

    [
        'event' => \App\Http\Event\Member\MemberLogin::class,
        'listeners' => [
            [
                "name" => \App\Http\Event\Member\Listener\ListenerA::class,
                'async' => true,
            ]
        ]
    ]

];
