<?php

return [
    'cors' => [
        'allow-origin' => \Framework\Foundation\Ini::get('cors.allow_origin', '*'),
        'allow-allow-credentials' => \Framework\Foundation\Ini::get('cors.allow_credentials', 'true'),
        'allow-headers' => \Framework\Foundation\Ini::get(
            'cors.allow_headers',
            'Content-Type,Content-Length, Authorization, Accept,X-Requested-With,token,DNT,Keep-Alive,User-Agent,Cache-Control'
        ),
        'allow-methods' => \Framework\Foundation\Ini::get('cors.allow_methods', 'GET, PUT, POST, DELETE, OPTIONS'),
    ],

    'json_web_token' => [
        'secret' => \Framework\Foundation\Ini::get('JWT.SECRET', 'uFLMFeKPPL525ppKrqmUiT2rlvkpLc9u'),
    ],

    'debug' => false,
];
